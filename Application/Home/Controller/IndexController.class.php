<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: yangweijie <917647288@qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Home\Controller;
use Michelf\MarkdownExtra;

/**
 * 前台首页控制器
 * 主要获取首页聚合数据
 */
class IndexController extends HomeController {

	//首页
    public function index(){
        $this->lists(I('get.page', 1));

        $this->display();
    }

    //详情
    public function detail($id){
        /* 标识正确性检测 */
        if(!($id && is_numeric($id))){
            $this->error('文档ID错误！');
        }

        /* 获取详细信息 */
        $Article = D('Article');
        $info = $Article->find($id);
        if($info['parse'] == 2){
            $markdown = new MarkdownExtra;
            $info['content'] = $markdown->transform($info['content']);
        }
        if(!$info){
            $this->error($Article->getError());
        }

        $tmpl = 'Index/detail';

        /* 更新浏览数 */
        $map = array('id' => $id);
        $Article->where($map)->setInc('view');

        /* 模板赋值并渲染模板 */
        $this->assign('info', $info);
        $this->display($tmpl);
    }

    //单页
    public function single($name){
        $article = D('Article')->getByName($name);
        if(!$article)
            $this->error('单页不存在');
        else{
            if(!empty($article['content'])){
                $this->assign($article);
                $this->display();
            }else{
                echo hook('single', array(
                    'name'=>$article['name'],
                    'title'=>$article['title'],
                ));
            }
        }
    }

    //分类
    public function category($name){
        $cate = D('Cate')->getByName($name);
        if(!$cate)
            $this->error('错误的分类');
        $this->assign('cate', $cate);
        $_GET['cate_id'] = $cate['id'];
        $this->lists(I('get.page',1));
        $this->display('Index/cate');
    }

    //归档
    public function archive($year, $month){
        $_GET['month'] = $month;
        $_GET['year'] = $year;
        $this->assign('year', $year);
        $this->assign('month', $month);
        $this->lists(I('get.page', 1));
        $this->display('Index/archive');
    }

    //搜索
    public function search(){
        $kw = I('get.kw');
        if(!$kw)
            $this->error('请输入关键字');
        $this->assign('kw', $kw);
        $this->lists(I('get.page', 1));
        $this->display('Index/search');
    }

}
