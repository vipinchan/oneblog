<?php

namespace Admin\Controller;

class ArticleController extends CommonController {

    public function index() {
        $map = array();
        $title = trim(I('title'));
        if ($title)
            $map['title'] = array('like', "%{$title}%");
        $map['cate_id'] = $_GET['cate_id'] = I('get.cate_id', 1);
        $map['type'] = I('get.type', 1);
        $this->cate_tree();
        $this->_list(array('source' => CONTROLLER_NAME, 'map' => $map, 'order' => 'sort asc, update_time desc'));

    }

    //上传封面
    public function coverUpload() {
        parent::ajaxUpload(
            array(
                'model'=>'Picture',
                'field'=>'cover',
            )
        );
    }

    public function add() {
        $_GET['cate_id'] = I('get.cate_id',0);
        $nav_tree = D('Cate')->select();
        $nav_tree = D('Tree')->toFormatTree($nav_tree);
        $this->assign('nav_tree', $nav_tree);
        session('uploaded', null);
        $this->assign('alltags', M('Tags')->select());
        $this->display();
    }

    public function edit($id) {
        $_GET['cate_id'] = I('get.cate_id',0);
        $_GET['model'] = CONTROLLER_NAME;
        $nav_tree = D('Cate')->select();
        $nav_tree = D('Tree')->toFormatTree($nav_tree);
        $this->assign('nav_tree', $nav_tree);
        $this->assign('alltags', M('Tags')->select());
        $this->_edit();
    }

    public function detail() {
        vendor('markdown');
        $chapter_id = I('cid');
        $chapterModel = D('Chapter');
        $chapter = $chapterModel->find($chapter_id);
        $chapter || $this->error('章节不存在！');
        $book = D(CONTROLLER_NAME)->find($chapter['book_id']);
        $book || $this->error('书籍不存在');
        if (IS_AJAX) {
            $this->ajaxReturn($chapter);
        } else {
            //获取目录列表
            $root_id = $chapterModel->getRoot($book['id']);
            $tree = $chapterModel->getDirectoryList($root_id);
            $tree = list_to_tree($tree, 'id', 'pid', '_', $root_id);
            $this->assign('chapter', $chapter);
            $this->assign('book', $book);
            $this->assign('tree', $tree);
            $this->display();
        }
    }

    public function editbleGetCate() {
        $nav_tree = D('Cate')->select();
        $nav_tree = D('Tree')->toFormatTree($nav_tree);
        $list = array();
        $list[] = array('value' => '', 'text' => '请选择');
        foreach ($nav_tree as $key => $value) {
            $list[] = array('value' => $value['id'], 'text' => $value['title_show']);
        }
        exit(json_encode($list));
    }

    //editble ajax更新方法
    public function ajaxUpdate() {
        $_POST = array(
            'id' => I('pk'),
            I('name') => I('value')
        );
        if(M('Article')->save($_POST) !== false)
            $this->success('');
        else
            $this->error('');
    }

    /**
     * 更新一条数据
     * @author huajie <banhuajie@163.com>
     */
    public function update($info = array('更新成功', '新增成功')){
        $res = D('Article')->update();
        if(!$res){
            $this->error(D('Article')->getError());
        }else{
            $this->success($res['id']?'更新成功':'新增成功');
        }
    }


}
