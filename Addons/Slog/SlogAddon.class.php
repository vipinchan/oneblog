<?php

namespace Addons\Slog;
use Common\Controller\Addon;

/**
 * Slog调试利器插件
 * @author yangweijie
 */

    class SlogAddon extends Addon{

        public $custom_config = 'config.html';
        public $info = array(
            'name'=>'Slog',
            'title'=>'Slog调试利器',
            'description'=>'用于异步调试php的神器',
            'status'=>1,
            'author'=>'yangweijie',
            'version'=>'0.1'
        );

        public function install() {
            if(APP_DEBUG){
                session('addons_install_error', ',slog插件需要在admin.php 中APP_DEBUG常量为false的情况下使用，不然会产生大量写日志操作');
                return false;
            }
            $sql = file_get_contents($this->addon_path . 'install.sql');
            $db_prefix = C('DB_PREFIX');
            $sql = str_replace('onethink_', $db_prefix, $sql);
            D()->execute($sql);
            $table_name = $db_prefix . 'slog';
            if (count(M()->query("SHOW TABLES LIKE '{$table_name}'")) != 1) {
                session('addons_install_error', ',slog表未创建成功，请手动检查插件中的sql，修复后重新安装');
                return false;
            }
            return true;
        }

        public function uninstall() {
            $db_prefix = C('DB_PREFIX');
            $sql = "DROP TABLE IF EXISTS `{$db_prefix}slog`;";
            D()->execute($sql);
            return true;
        }

        //实现的app_begin钩子方法
        public function app_begin($param){
            require_once __DIR__.'/function.php';
        }

    }