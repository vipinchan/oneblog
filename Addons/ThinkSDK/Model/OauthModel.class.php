<?php

namespace Addons\ThinkSDK\Model;
use Think\Model;
/**
 * MovieLog模型
 */
class OauthModel extends Model{

    public $model = array(
        'title'=>'sns绑定',//新增[title]、编辑[title]、删除[title]的提示
        'template_add'=>'',//自定义新增模板自定义html edit.html 会读取插件根目录的模板
        'template_edit'=>'',//自定义编辑模板html
        'search_key'=>'type',// 搜索的字段名，默认是title
        'extend'=>1,
    );

}
