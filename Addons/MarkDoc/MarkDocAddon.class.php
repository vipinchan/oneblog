<?php

namespace Addons\MarkDoc;
use Common\Controller\Addon;
use Behavior\ContentReplaceBehavior;

/**
 * 马克笔记插件
 * @author yangweijie
 */

    class MarkDocAddon extends Addon{

        public $info = array(
            'name'=>'MarkDoc',
            'title'=>'马克笔记',
            'description'=>'基于Daux.io功能制作的一个在线访问目录式笔记的工具。',
            'status'=>1,
            'author'=>'yangweijie',
            'version'=>'0.1'
        );

        public function install(){
            $hookModel = M('Hooks');
            if(!$hookModel->where('name = "single"')->find()){
                $hookModel->add(array(
                    'name'=>'single',
                    'description'=>'单页面专门用的钩子',
                    'type'=>1,
                    'update_time'=>time()
                    )
                );
                S('hooks', null);
                if(!$hookModel->where('name = "single"')->find())
                    session('addons_install_error', 'single钩子创建失败');
            }
            return true;
        }

        public function uninstall(){
            return true;
        }

        //实现的single钩子方法
        public function single($param){
            if($param['name'] == 'MarkDoc'){

                require_once ("{$this->addon_path}function.php");
                $MarkDoc = new \Addons\MarkDoc\Model\MarkDoc($this->addon_path);

                $options = get_options();
                $base_path = "{$this->addon_path}{$options['docs_path']}";
                if(S('MarkDoc_url_tree') && filemtime($base_path) < S('MarkDoc_LAST_BUILD')){
                    $tree = S('MarkDoc_url_tree');
                }else{
                    $tree = get_tree($base_path, $base_url);
                    S('MarkDoc_url_tree', $tree);
                    S('MarkDoc_LAST_BUILD', NOW_TIME);
                }
                // If a language is set in the config, rewrite urls based on the language
                if (! isset($language) || $language === null) {
                    $homepage_url = $MarkDoc->homepage_url($tree);
                    $docs_url = docs_url($tree);
                } else {
                    $homepage_url = "/";
                }

                $docs_url = docs_url($tree);
                $url_params = $MarkDoc->url_params();
                if (count($options['languages']) > 0 && count($url_params) > 0 && strlen($url_params[0]) > 0) {
                    $language = array_shift($url_params);
                    $base_path = $base_path .'/'. $language;
                } else {
                    $language = null;
                    $base_path = $base_path;
                }

                if(S('MarkDoc_tree') && filemtime($base_path) < S('MarkDoc_LAST_BUILD')){
                    $tree = S('MarkDoc_tree');
                }else{
                    $tree = get_tree($base_path, '/Index/single/name/MarkDoc', '', true, $language);
                    S('MarkDoc_tree', $tree);
                    S('MarkDoc_LAST_BUILD', NOW_TIME);
                }

                $page = $MarkDoc->load_page($tree, $url_params, $param['title']);

                //替换手册里的图片地址
                $TMPL_PARSE_STRING = C('TMPL_PARSE_STRING');
                $TMPL_PARSE_STRING['src="images'] = "src=\"{$TMPL_PARSE_STRING['__ADDONROOT__']}/{$options['docs_path']}/images";
                C('TMPL_PARSE_STRING', $TMPL_PARSE_STRING);
                $cr = new ContentReplaceBehavior();
                $cr->run($page['html']);

                if(isset($options['timezone'])){
                    date_default_timezone_set($options['timezone']);
                }

                // Redirect to docs, if there is no homepage
                if ($homepage && $homepage_url !== '/') {
                    header('Location: '.$homepage_url);
                }
                $this->assign('homepage', $homepage);
                $this->assign('url_params', $url_params);
                $this->assign('tree', $tree);
                $this->assign('page', $page);
                $this->assign('options', $options);
                $this->assign('homepage_url', $homepage_url);
                $this->display('list_1');
            }
        }

    }
