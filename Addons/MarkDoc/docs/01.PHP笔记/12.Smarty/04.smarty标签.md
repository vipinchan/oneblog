
变量标签
=========

> smarty标签是smarty预置的插件

{assign}
----------

> 用于创建一个新模板变量

|属性       |数据类型|必须 |默认值|解释                                     |
|-----------|------|-----|----|-----------------------------------------|
|var        |string|Yes  |n/a |变量名                                   |
|value      |string|Yes  |n/a |变量值                                   |
|scope      |string|No   |n/a |变量有效范围: 'parent','root' or 'global'|


* parent：在父模板中有效
* root:在整个引用树中有效
* global:在所有模板中有效

{append}
---------

> 向数组中添加新元素

|属性       |数据类型|必须 |默认值|解释                        |
|-----------|------|-----|----|----------------------------|
|var        |string|Yes  |n/a |数组变量名                  |
|value      |string|Yes  |n/a |元素值                      |
|index      |string|No   |n/a |元素键名(如果不指定键名，则以索引元素添加到数组末尾)|
|scope      |string|No   |n/a |:'parent','root' or 'global'|


{config_load}
------------------

> 用于从配置文件中 加载配置变量#variables#。

|属性       |数据类型|必须 |默认值|解释                                            |
|-----------|------|-----|-----|------------------------------------------------|
|file       |string|Yes  |n/a  |载入的配置文件名                                |
|section    |string|No   |n/a  |指定载入配置变量的段落                          |
|scope      |string|no   |local|配置变量的作用范围，取值local, parent 或 global.|


* local表示变量只能在当前模板的上下文中使用。 
* parent表示变量可以在当前模板和父模板使用。 
* global表示变量在任何地方都可用

{capture}
--------------

> 可以捕获标记范围内的输出内容，存到$samrty.capture变量或指定的自定义变量或添加到自定义数组中而不显示。

|属性       |数据类型|必须 |默认值|解释                        |
|-----------|------|-----|----|----------------------------|
|name       |string|Yes  |n/a |为这个捕获区域命名          |
|assign     |string|No   |n/a |将捕获的内容赋值给自定义变量|
|append     |string|No   |n/a |将捕获的内容增加到数组中    |


输出保存在$smarty.capture中的捕获：

```
{capture "banner"}
  {include file="get_banner.tpl"}
{/capture}

{if $smarty.capture.banner ne ""}
    {$smarty.capture.banner}
{/if}
```

语言结构插件
===========

> {y:语言结构插件必须有闭合标签}

{for}{forelse}
------------

> 完整语法：`{for $var=$start to $end step $step}`

> 简短语法：`{for $var=$start to $end}` step为1

|属性|Shorthand|Type   |Required|Default|Description|
|---|-----------|-----|--------|------|----------|
|max|n/a      |integer|No      |n/a    |循环的最大次数|


```
<ul>
{for $i=1 to $end step 1 max=100}
    <li>{$i}</li>
    {forelse}
      no iteration
{/for}
</ul>
```


{if},{elseif},{else}
--------------------

条件表达式与php中语法一样

```
{if isset($name) && $name == 'Blog'}
     {* do something *}
{elseif $name == $foo}
    {* do something *}
{/if}

{if is_array($foo) && count($foo) > 0}
    {* do a foreach loop *}
{/if}
```


{foreach},{foreachelse},{break},{continue}
----------------------

> 语法与php相同，可以遍历数组或迭代器。也可以被嵌套使用；

> 如果array数组变量中没有值{foreachelse}将执行。

###元素@属性：

> 遍历时，不仅可以得到当前元素，还可以得到当前元素的一些属性状态。使用时，直接跟在元素值变量之后即可

### @index

当前元素的索引号

```
<table>
{foreach $items as $i}
  {if $i@index eq 3}
     {* put empty table row *}
     <tr><td>nbsp;</td></tr>
  {/if}
  <tr><td>{$i.label}</td></tr>
{/foreach}
</table>
```

### @iteration

当前迭代的序号，从1开始，也就是当前是第几次循环

```
{foreach $myNames as $name}
  {if $name@iteration is div by 4}
    <b>{$name}</b>
  {/if}
  {$name}
{/foreach}
```

### @first

是否是第一次循环，返回值为布尔值

### @last

是否是最后一次循环，返回值为布尔值

### @show

用在循环体之后，用来判断循环过程是否输出过数据。

```
<ul>
{foreach $myArray as $name}
    <li>{$name}</li>
{/foreach}
</ul>
{if $name@show}
    do something here if the array contained data
{/if}
```

### @total

整个{foreach}循环的次数。 total可以在{foreach}循环体内部，或者在循环体之后。

{break}, {continue}用在循环体内

{section},{sectionelse}
------------------------

{section}可以循环遍历 连续数字索引的数组,一般更推荐使用{foreach}语法

{while}
-----------

Smarty的{while}循环和PHP的while 几乎一样的，只是每个{while}必须有相应的{/while}. 任何的PHP条件或者函数表达式都是可用的，如`||, or, &&, and, is_array()` 等等

模板复用
==========

{include}
----------

用于载入其他模板到当前模板中。 在包含模板中可用的变量，载入后在当前模板仍然可用。

|属性          |数据类型      |必须 |默认值|解释                                                         |
|--------------|--------------|-----|-----|-------------------------------------------------------------|
|file          |string        |Yes  |n/a  |包含载入的文件名                                             |
|assign        |string        |No   |n/a  |将包含的文件内容赋值给变量                                   |
|cache_lifetime|integer       |No   |n/a  |单独开启被包含模板的缓存时间                                 |
|compile_id    |string/integer|No   |n/a  |单独设置被包含模板的编译ID                                   |
|cache_id      |string/integer|No   |n/a  |单独设置被包含模板的缓存ID                                   |
|scope         |string        |No   |n/a  |定义被包含模板的赋值变量作用范围: 'parent','root' 或 'global'|
|[var ...]     |[var type]    |No   |n/a  |传递到包含模板的变量                                         |
|nocache       |bool          |No   |false|关闭包含模板的缓存                                           |
|caching       |bool          |No   |false|打开包含模板的缓存                                           |
|inline        |bool          |No   |false|设置成true时，在编译时把包含模板的内容也合并到当前模板的编译文件中。|

{extends}
----------

> 模板继承中，你可以在子模板内使用{extends file="xxx.tpl"}标签来扩展父模板。

!!!!!{extends}必须放在模板的第一行。

扩展继承意味着子模板可以覆盖部分或全部父模板的块区域。

模板继承在编译时将编译成单独的一个编译文件，比{include}的性能更高。

{block}
----------

{block}可在模板上定义一块区域，这个区域的内容将被子模板中同名的{block}的覆盖，或添加到前面或后面。就像php中的类继承一样。

####属性:

|属性       |数据类型|必须 |默认值|解释          |
|-----------|------|-----|----|--------------|
|name       |string|Yes  |n/a |模板区域的名称|


####可选属性 (仅在子模板中使用):

|名称   |说明                                          |
|-------|----------------------------------------------|
|append |{block}区域代码将附加到父模板的{block}内容之后|
|prepend|{block}区域代码将附加到父模板的{block}内容之前|
|hide   |在没有该名称区域的时候，忽略区域内容。        |
|nocache|关闭{block} 缓存                              |

###模板继承实例

```
{extends file='layout.tpl'}
{block name=head}
  <link href="/css/mypage.css" rel="stylesheet" type="text/css"/>
  <script src="/js/mypage.js"></script>
{/block}
```

模板函数
==========

> 对于某些可复用的模板逻辑（例如输出菜单），可能太简单以至于没有必要单独创建一个插件。那么直接在模板中创建一个函数来执行这些可复用的逻辑。

{function}
-----------

> 用于在模板内创建函数

> 调用：与插件的调用方式相同，也可以使用{call}调用

|属性            |数据类型|必须 |默认值|解释                |
|----------------|------|-----|----|--------------------|
|name            |string|Yes  |n/a |模板函数的名称      |
|[var=value, ...]|[标量]|No   |n/a |模板函数参数的默认值|

```
{* 定义模板函数 *}
  <ul class="level{$level}">
  {foreach $data as $entry}
    {if is_array($entry)}
      <li>{$entry@key}</li>
      {menu data=$entry level=$level+1}
    {else}
      <li>{$entry}</li>
    {/if}
  {/foreach}
  </ul>
{/function}
{$menu = ['item1','item2','item3' => ['item3-1','item3-2','item3-3' =>['item3-3-1','item3-3-2']],'item4']}
{* 调用模板函数 *}
{menu data=$menu}
```

其他
----------

### {debug}

会在页面上显示调试控制台。 它将忽略PHP代码中的debug配置。 

###{nocache}{/nocache}

用于关闭模板区块的缓存。

###{strip}{/strip}

任何在`{strip}{/strip}`中的标签都会被过滤掉多余的空格和回车， 并把全部标签放在同一行内以后才发送给浏览器，有助于减小流量，避免ie6的<li>间距bug