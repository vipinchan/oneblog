
颜色分配
------------

<p class="api">int imagecolorallocate (resource $image, int $red, int $green, int $blue )</p>

为一幅图像分配颜色

imagecolorallocate() 返回一个标识符，参数是 0 到 255 的整数或者十六进制的 0x00 到 0xFF。
 
!!!!第一次对 imagecolorallocate()的调用会给基于imagecreate() 建立的图像填充背景色，不会给真彩色图像填充背景色。

<p class="api">int imagecolorallocatealpha (resource $image,int $red,int $green,int $blue,int $alpha)</p>

参数 alpha，其值从 0 到 127。{y:0表示完全不透明，127 表示完全透}

颜色填充
----------

<p class="api">bool imagefill ( resource $image, int $x, int $y, int $color )</p>

用颜色或贴图填充区域 

在 image图像的坐标 x，y（图像左上角为 0, 0）处用 color颜色执行区域填充（即与 x, y 点颜色相同且相邻的点都会被填充）。 

<p class="api">bool imagefilltoborder (resource $image,int $x,int $y,int $border,int $color )</p>

用颜色或贴图填充区域到边界颜色

imagefilltoborder() 从 x，y（图像左上角为 0, 0）点开始用 color颜色执行区域填充，直到碰到颜色为 border的边界为止。

!!!!注：边界内的所有颜色都会被填充。如果指定的边界色和该点颜色相同，则没有填充。{r:如果图像中没有该边界色，则整幅图像都会被填充}




