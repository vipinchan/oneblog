访问操作符
----------

### 对象解析操作符( -> ) 

访问对象的属性或方法

### 范围解析操作符（::）

在类的外部通过Classname::直接访问类代码中的静态属性、常量和成员方法区的方法代码。
在类的内部可以使用self::,parent::,class::,static::
 
|可访问性|->                    	|::               | 
|--------|------------------------|-----------------|
|方法    |可以{r:访问任何上下文中可见的方法}|只能访问静态方法(或者没有使用$this指针的普通方法，但会导致一个E_STRICT级别的错误) 
|属性    |只能访问非静态属性,不能访问静态属性      |只能访问静态属性,不能访问对象属性 |
|常量    |不能访问常量            |可以访问常量     |
|连续调用|支持                    |{r:不支持}           |
|访问过程|通过对象变量进行        |通过类名直接访问 |

self和parent的工作原理
--------------------

静态方法和非静态方法，都是存在zend_class_entry的同一个字段function_table，仅依靠static关键字区别。

正是这个原因`->`也可以访问静态方法，`::`也可以访问非静态方法(但是会报Strict错误，遇到$this时，则会包fatal error并终止程序)。

{y:可以这么做不代表应该这么做，程序员应严格遵守`->`访问普通方法和`::`访问静态方法的约定。}

`->`操作符告诉php上下文环境是对象，访问目标是对象方法和属性，在找不到对象方法时，找同名的非静态方法,但始终不能访问静态属性

`::`操作符告诉php上下文环境是类，访问目标是静态方法和常量，但`parent::`,`self::`稍有特殊。

关于self和parent
----------------------------

* **parent,self**不是魔术常量，而是有功能性的关键字！所以，{r:`self`不等于`__CLASS__`，`parent`不等于`get_parent_class(__CLASS__)`}
* **self**和**parent**的功能就是{g:携带上下文信息}，php优先使用它们的上下文信息，parent携带的上下文信息是对象的话，php认为你想访问的是普通方法，否则认为你要调用的是静态方法
* self 和 parent 无法访问静态属性!

```php
$classname::constant;   //类的动态调用
classname::$$my_static; //静态属性的动态调用
Myclass::$methodname(); //静态方法的动态调用
//要动态调用静态成员方法，还可以用call_user_func和call_user_func_array，forward_static_cal，forward_static_cal_array

// ::符号不能连续调用
//self::$log::write(); $this->log::write();// 解析错误
```

$this指针的传递
---------------

> 当一个对象实例化创建后，一个执行该对象的指针$this便同时生成。此后，对对象方法访问时，$this指针将自动传入方法内，{y:除非访问的是另一个**对象**中的方法。}

> 在继承体系中,$this将首先试图访问自己所处类定义过private属性和方法,如果没有找到,则从对象的子类依次查找访问目标

```php
class CalledClass{
    function go(){
        print(get_class($this) . "\n");
    }
}
                   
class CallerClass{
    function go(){
        CalledClass::Go();
    }
}
$obj = new CallerClass();
$obj->go();

//访问过程中，始终没有遇到其他对象的方法，所以$this始终都是指向$obj                   
// Output is "CallerClass"
```

```php
class CalledClass{
    function go(){
        print(get_class($this) . "\n");
    }
}
                  
class CallerClass{
    function go(){
        $a=new CalledClass();
        $a->Go();
    }
}
$obj = new CallerClass();
$obj->go();

//访问过程中遇到了另一个对象$a,此后，$this指向了$a                  
// Output is "CalledClass"
```



后期静态绑定
-------------

> static::绑定的是最近一次使用`classname::method()`或者`$obj->method()`的那个classname或$obj的类名。

绑定过程：A::func_a()  那么func_a()中的stactic被绑定为A；如果func_a()代码中含有B:func_b()，那么func_b()中的static又被绑定为B

{r:self和parent不参与后期静态绑定}，只有Classname::，$obj->才会参与后期静态绑定，Classname允许以变量传递。

```php
class A {
    public static function foo() {
        static::who();
    }
    public static function who() {
        echo __CLASS__."\n";
    }
}
        
class B extends A {
    public static function test() {
        A::foo(); //很明显，绑定变更为A
        $c="A";
        $c::foo(); //绑定变更为$c的值，A
        $b=  new A();
        $b->foo(); //绑定变更为对象$b的所属类A
        parent::foo(); //不参与绑定，绑定的依然是C
        self::foo(); //不参与绑定，绑定的依然是C
    }
    public static function who() {
        echo __CLASS__."\n";
    }
}
class C extends B {
    public static function who() {
        echo __CLASS__."\n";
    }
}
C::test(); //最初static绑定为C
?>
输出：
A
A
A
C
C
```
