> 以下函数都会移动数组指针

<p class="api">
mixed reset(array &$array); 
</p>

把数组指针移动到第一个元素上，返回值为第一个元素的值

<p class="api">
mixed end(array &$array); 
</p>

把数组指针移动到最后一个元素上，返回值为最后一个元素的值 

<p class="api">
mixed next(array &$array); 
</p>

把数组指针移动到下一个元素，返回值为下一个元素的值，如果移动到尾部返回布尔值false。

<p class="api">
mixed prev ( array &$array )
</p>

把数组指针移动到上一个元素，返回值为上一个元素的值，如果移动到头部返回布尔值false。


<div class="alert alert-warning">
由于数组的元素值有可能是0,'0',null,'',array()，但并不表示已经到了末尾。所以，判断next()和prev()是否已经到底末端或顶端，请使用 === 严格等号。<br>
如果数组中含有false元素，则使用next()和prev()的返回值进行判断将不准确！对于这种情况，要么先过滤数组，要么使用each()
</div>

> 以下函数不会移动指针

<p class="api">
mixed current(array &$array); 别名pos() 
</p>

返回当前指针所在位置的元素值 ，并不会移动指针。

<p class="api">
mixed key(array &$array); 
</p>

返回当前指针所在位置的元素键名，并不会移动指针。

数组遍历时，是根据指针进行移动，而不是根据索引。

如果数组包含空的单元，或者单元的值是 0 则本函数碰到这些单元也返回 FALSE。要正确遍历可能含有空单元或者单元值为 0 的数组，参见 each() 函数