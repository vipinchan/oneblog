基础
===========

> 官网:　<http://www.tcpdf.org>

> 功能：可以从文本或html创建PDF文档，支持页眉页尾，目录大纲等众多功能，生成的文档自适应文档页宽．

字体
------------

TCPDF支持unicode字体,但只支持TTF字体,不支持TTC字体.

基本使用
------------

```php

//引入tcpdf.php
require_once('./tcpdf/tcpdf.php');
//创建TCPDF实例
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
//字体设置
//$fontname='yahei_mono';//如果字体已经创建过,可以直接使用字体名
$fontname = $pdf->addTTFfont('/home/zhuyajie/.fonts/yahei_mono.ttf', 'TrueTypeUnicode', '', 32);

/*
 * 各种文档设置
 */

//自动分页
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
//创建页面
$pdf->AddPage();
$html = file_get_contents('/path/to/your/file.html');
// 转换html
$pdf->writeHTML($html, true, false, true, false, '');

// 文档结束
$pdf->lastPage();
// 保存文档到文件
$pdf->Output('test.pdf', 'F');
```

文档配置
---------

`tcpdf.php` 会载入 `tcpdf_autoconfig.php` 执行默认配置, 首先会设置几项关键配置:
