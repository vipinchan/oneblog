运算符
==============

算术运算符 
----------


> ` + - * / %`(加减乘除求模)

## 算术运算返回类型

* 除法运算符总是返回浮点型，以下情况例外：
	
	两个操作数都是整数（或字符串转换成的整数）并且正好能整除，这时它返回一个整型。

* 取模运算符的操作数在运算之前都会转换成整数（floor方式丢弃小数位）， 取模 $a % $b 在 $a 为负值时的结果也是负值。

* `+ - * `运算的两边如果有浮点型，则返回结果为浮点型



递增递减运算符 
--------------

> 优先级非常高，仅次于clone和new

`$a += 1;`等价于`$a=$a+1;`

$a++;//先返回$a，再执行$a自身的递增

	例如：$c = $b + $a++;先执行的是$c=$b+$a,执行完以后，对$a递增

++$a;//先对$a执行递增，然后再返回$a

	例如：$c = $b + ++$a;先执行的是$a递增，然后执行$c=$b+$a

!!!!!递增递减运算符可以作用于：整型、浮点型(仅递增整数部分)、英文字符串、null(转化为0)，但对布尔型不起作用

**++  --**是独立的运算符，并不是$a+=1的缩写（所以他们对字符串的处理处理方式不同）

对于最后一个字符是英文的字符串，++ --会把从字符串的最后一个英文字符开始运算。规律见http://note.sdo.com/u/1665079196/n/zfNus~k5tbBVnM2gk000w-



比较运算符 
----------

|符号  |类型    |
|------|--------|
|>	   |大于    |
|<	   |小于    |
|>=    |大于等于| 
|<=    |小于等于| 
|<> ,!=|先把两边转换为同一类型，再进行比较|
|!==   |首先比较变量类型，如果变量类型相同再比较值|
|==    |等于    |
|===   |全等于  |


逻辑运算符 
---------

|符号表示|单词表示|
|------|--------|
|&& |and|
|\|\||or |
|!  |not|
|无 |xor (只有当两边有且仅有一个为真时，值为真，其余全为假)|
 

位运算符 
-------

`& | ^ <<  >>`


其他运算符
----------

### 连接运算符`.=`

例如： `$a .= 3//等价于$a=$a.3 `

### @运算符

禁止显示警告信息，{r:不可以放在条件语句中 }

### 三元表达式

表达式1？表达式2：表达式3

当表达式1为真时执行表达式2，否则执行表达式3 

运算符的优先级
==============

主要需要记住的就是{r:赋值运算符的优先级几乎最低}，仅仅比“and xor or”的优先级高

{r:!的优先级非常高，仅低于关键字和++ --}

`&&`的优先级大于`||`，表达式混合有`&&`和`||`时，最好使用括号来规定优先顺序，避免混乱


|结合方向|运算符     |	附加信息    |
|--------|-----------|--------------|
|非结合  |clone new  |	clone 和 new|
|左	     |[          |	array()     |
|非结合  |	++ --	 |递增／递减运算符|
|非结合  |	~ - (int) (float) (string) (array) (object) (bool) @|类型|
|非结合  |instanceof |类型          |
|右结合  |	!	     |逻辑操作符    |
|左      |	* / %	 |算术运算符    |
|左      |	+ - .	 |算术运算符 和 字符串运算符|
|左      |	<< >>	 |位运算符      |
|非结合  |	< <= > >= <>|比较运算符 |
|非结合  |	== != === !==|比较运算符|
|左      |	&	|位运算符 和 引用|
|左      |	^	|位运算符|
|左      |	\|	|位运算符|
|左      |	&&	|逻辑运算符|
|左      |	\|\|	|逻辑运算符|
|左      |	? :	|三元运算符|
|右      |	= += -= *= /= .= %= &= \|= ^= <<= >>=	|赋值运算符|
|左      |	and	|逻辑运算符|
|左      |	xor	|逻辑运算符|
|左      |	or	|逻辑运算符|
|左      |	,   |-|

递增递减运算规律
================

* 对null进行递增递减，会把null转换为0后执行

* 对浮点型进行递增递减，则对整数部分进行递增递减，小数部分保持不变

* 对布尔值递增递减，不会对变量做任何改变，即，无效果。

* 对字符串递增递减：

	只对最后一个字符是英文字符的字符串有效果，对其他字符串没有效果，也不会发生类型转换
	
	如果最后一个字符是小写英文字符，则递增递减的有效范围在a-z范围内，当递减至a以后，继续递减则不会有效。当递增至z以后继续递增1，最后一个字符则变为了a，倒数第二个英文字符递增1(如果倒数第二个字符不是英文字符，则倒数第二个字符不变，只最后一个字符变化)。

	如果最后一个字符是大写英文字符，则递增递减的有效范围在A-Z范围内，当递减至A以后，继续递减则不会有效。当递增至Z以后继续递增1，最后一个字符则变为了A，倒数第二个英文字符递增1(如果倒数第二个字符不是英文字符，则倒数第二个字符不变，只最后一个字符变化)。
