闭包
========

> 又叫匿名函数,是php5.3引入的新特性,并在5.4得到增强

> 闭包函数可以通过使用use从被创建的上下文向闭包内传递

```
<?php

function test(){
	$t1 = 'aaa';
	$t2 = 'bbb';
	$a = function() use($t1,&$t2) {
			dump($t1);
			$t1='bbb';
			$t2='ccc';
		};
	call_user_func($a);
	dump($t1);
	dump($t2);
}

test();
```


php5.4支持在闭包内使用$this

```
class test {
	public $t1 = 'aaa';

	function a(){
		$a = function() {
				dump($this->t1);
				$this->t1='bbb';
			};
		call_user_func($a);
		dump($this->t1);
	}
}

$a = new test;
$a->a();
```

