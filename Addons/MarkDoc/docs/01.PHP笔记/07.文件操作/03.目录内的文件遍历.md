函数式操作
-----------

<p class="api">resource opendir (string $path [,resource $context ])</p>

######打开目录,获得一个目录句柄，可用于之后的 closedir()，readdir() 和 rewinddir() 调用中。

* 目录打开状态不能执行删除rmdir()
* path 支持 ftp:// URL wrappe

<p class="api">string readdir ( resource $dir_handle )</p>

* 返回目录中下一个文件的文件名。文件名以在文件系统中的排序返回。

* 当没有文件时返回false，判断时应使用` === `

<p class="api">void closedir ( resource $dir_handle )</p>

关闭已打开的目录资源

<p class="api">void rewinddir ( resource $dir_handle )</p>

将指针重置到目录资源头部

OO式操作
----------------

!!!!!Directory对象使用dir()函数返回，不能使用new操作创建

```php
$dir=dir('.'); //根据参数返回一个Directory对象
$dir->read();  //读取目录内容方法
$dir->rewind();  //重置目录指针方法
$dir->close(); //关闭目录
```

#####!!判断一个目录是否为空目录

```php
//推荐方法
function is_empty_dir($ddir){
    $d=opendir($ddir);
    $i=0;
    while($a=readdir($d)&& $i<=2){
        $i++;
    }
    closedir($d);
    if($i>2){return false;}
    else {return true};
}
```

```php
//不推荐(scandir会扫描所有文件返回一个数组，这比较占资源)
function   isEmptyDir(   $path   )
{
        $dh=   scandir(   $path   ); 
        if(2==count($dh)){
               return false;
        }
        return   true;
}
```