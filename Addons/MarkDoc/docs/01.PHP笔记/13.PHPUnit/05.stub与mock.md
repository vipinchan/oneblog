Build mock
=========

> 可以使用两种方式Build mock


getMock()
------------

<p class="api">getMock($originalClassName,$methods=array(),array $arguments=array(),$mockClassName=”,$callOriginalConstructor=TRUE,$callOriginalClone=TRUE,$callAutoload=TRUE)</p>

* 第一个参数:是原始类的类名
* 第二个参数:只有在数组中的方法才会被mock,不在数组中的方法不改变其行为
* 第三个参数:该数组向原始类传递构造方法需要的参数
* 第四个参数:为mock类起个类名
* 第五个参数:用来禁止调用原始类的构造函数
* 第六个参数:用来禁止调用原始类的__clone方法
* 第七个参数:用来禁止调用原始类的__autoload方法 此外，可以使用Mock Builder API来配置mock对象

Mock Builder API
-----------------

> 链式api

* getMockBuilder($class)   第一个调用
* setMethods(array $methods)
* setConstructorArgs(array $args)
* setMockClassName($name)
* disableOriginalConstructor()
* disableOriginalClone()
* disableAutoload()
* getMock()	最后一个调用

```php
$stub = $this->getMockBuilder('SomeClass')
                     ->disableOriginalConstructor()
                     ->getMock();
```


如果中bulid mock时没有限定method，则目标类的所有未使用will配置返回值的方法都会被配置成返回值为NULL。


stub和mock
==========


Stub
-----------

> stub对象就像骗子,冒充自己就是目标类的实例。并且可以配置所冒充的方法的返回值。

```php
<?php
class SomeClass{
    public function doSomething(){
    }
}
?>
```

上面的类是项目中的一个类，注意，我们还没有为doSomething()写任何实际代码,所以它的实际返回值为null。 

下面是测试类代码

```php
<?php
require_once 'SomeClass.php';

class StubTest extends PHPUnit_Framework_TestCase
{
    public function testStub()
    {
        // 为 SomeClass 类创建一个stub对象
        $stub = $this->getMock('SomeClass');

        // 配置这个stub对象中的doSomething方法的虚拟返回值
        $stub->expects($this->any())
             ->method('doSomething')
             ->will($this->returnValue('foo'));

        // 调用 $stub->doSomething() 将返回配置的返回值
        $this->assertEquals('foo', $stub->doSomething());
    }
}
?>
```

在测试目标中，调用了getMock方法来得到一个虚构的 SomeClass对象$stub

method(‘doSomething’) 这句声明了”doSomething”方法为mock方法

will($this->returnValue(‘foo’)) 为 $stub->doSomething()方法虚构了一个返回值foo

!!!!!上例中，method() 和 $this->returnValue() 的参数只有一个，使用了字符串。如果要声明多个mock方法，则method()和returnValue()的参数都使用数组，按索引顺序对应。

will方法的参数
--------------

### $this->returnValue()

配置stub方法有明确的返回值

### $this->returnSelf()

配置stub方法返回自身$stub对象的引用

```php
<?php
require_once 'SomeClass.php';

class StubTest extends PHPUnit_Framework_TestCase
{
    public function testReturnSelf()
    {
        // Create a stub for the SomeClass class.
        $stub = $this->getMock('SomeClass');

        // Configure the stub.
        $stub->expects($this->any())
             ->method('doSomething')
             ->will($this->returnSelf());

        // $stub->doSomething() returns $stub
        $this->assertSame($stub, $stub->doSomething());
    }
}
?>
```
###$this->returnArgument($index)

配置stub方法返回stub方法调用时索引位置的参数（0是第一个参数）


```php
<?php
require_once 'SomeClass.php';

class StubTest extends PHPUnit_Framework_TestCase
{
    public function testReturnArgumentStub()
    {
        $stub = $this->getMock('SomeClass');

        $stub->expects($this->any())
             ->method('doSomething')
             ->will($this->returnArgument(0));

        $this->assertEquals('foo', $stub->doSomething('foo'));

        $this->assertEquals('bar', $stub->doSomething('bar'));
    }
}
?>
```

### $this->returnValueMap(array $map)

用来定义一组映射参数，根据映射关系返回值,$map的数组的一维元素都是含有一组参数和一个返回值的数组（最后一个元素是返回值，前面的元素是参数）

调用stub方法时候，查询传入的参数与$map中的哪一个配置匹配，返回该匹配的返回值

```php
<?php
require_once 'SomeClass.php';

class StubTest extends PHPUnit_Framework_TestCase
{
    public function testReturnValueMapStub()
    {
        $stub = $this->getMock('SomeClass');

        $map = array(
          array('a', 'b', 'c', 'd'),
          array('e', 'f', 'g', 'h')
        );

        $stub->expects($this->any())
             ->method('doSomething')
             ->will($this->returnValueMap($map));

        $this->assertEquals('d', $stub->doSomething('a', 'b', 'c'));
        $this->assertEquals('h', $stub->doSomething('e', 'f', 'g'));
    }
}
?>
```

### $this->returnCallback(callable $callback)

用回调函数的返回值将作为stub方法的返回值,回调函数的接收的参数与stub方法的参数一致。

```php
<?php
require_once 'SomeClass.php';

class StubTest extends PHPUnit_Framework_TestCase
{
    public function testReturnCallbackStub()
    {
        $stub = $this->getMock('SomeClass');

        $stub->expects($this->any())
             ->method('doSomething')
             ->will($this->returnCallback('str_rot13'));

        // $stub->doSomething($argument) returns str_rot13($argument)
        $this->assertEquals('fbzrguvat', $stub->doSomething('something'));
    }
}
?>
```

### $this->onConsecutiveCalls()

该方法可以传入多个参数作为每次调用stub方法时的返回值。

```php
<?php
require_once 'SomeClass.php';

class StubTest extends PHPUnit_Framework_TestCase
{
    public function testOnConsecutiveCallsStub()
    {
        $stub = $this->getMock('SomeClass');

        $stub->expects($this->any())
             ->method('doSomething')
             ->will($this->onConsecutiveCalls(2, 3, 5, 7));

        $this->assertEquals(2, $stub->doSomething());
        $this->assertEquals(3, $stub->doSomething());
        $this->assertEquals(5, $stub->doSomething());
    }
}
?>
```


### $this->throwException(Exception $e)

配置stub方法抛出一个异常实例

```php
<?php
require_once 'SomeClass.php';

class StubTest extends PHPUnit_Framework_TestCase
{
    public function testThrowExceptionStub()
    {
        // Create a stub for the SomeClass class.
        $stub = $this->getMock('SomeClass');

        // Configure the stub.
        $stub->expects($this->any())
             ->method('doSomething')
             ->will($this->throwException(new Exception));

        // $stub->doSomething() throws Exception
        $stub->doSomething();
    }
}
?>
```


Mock对象
----------

通过stub对象，我们可以虚构一个目标实例，最后调用will()方法为Stub方法配置期望返回值。

但只有这些往往还不够，因为测试有时可能还需要关注这个对象内的方法执行的情况,例如需要关注方法执行了几次，什么时候执行的，执行时的参数是否符合要求。

mock对象就像在目标对象内的间谍,在幕后监视着目标方法的参数约束和执行情况。{y:通过expects()方法为mock方法配置期望的调用次数，通过**with()**方法为mock方法的参数配置约束。}


### 实例

```php
<?php
class Subject
{
    protected $observers = array();

    public function attach(Observer $observer)
    {
        $this->observers[] = $observer;
    }

    public function doSomething()
    {
        // Notify observers that we did something.
        $this->notify('something');
    }

    public function doSomethingBad()
    {
        foreach ($this->observers as $observer) {
            $observer->reportError(42, 'Something bad happened', $this);
        }
    }

    protected function notify($argument)
    {
        foreach ($this->observers as $observer) {
            $observer->update($argument);
        }
    }

    // Other methods.
}

class Observer
{
    public function update($argument)
    {
        // Do something.
    }

    public function reportError($errorCode, $errorMessage, Subject $subject)
    {
        // Do something
    }

    // Other methods.
}
?>
```

上面是一个简单的观察者模式类，下面是一个测试类

```php
<?php
class SubjectTest extends PHPUnit_Framework_TestCase
{
    public function testObserversAreUpdated()
    {
        // only mock the update() method.
        $observer = $this->getMock('Observer', array('update'));

        // Set up the expectation for the update() method
        // to be called only once and with the string 'something'
        // as its parameter.
        $observer->expects($this->once())
                 ->method('update')
                 ->with($this->equalTo('something'));

        $subject = new Subject;
        $subject->attach($observer);
        $subject->doSomething();
    }
}
?>
```


在上面的测试中，with()使用$this->equalTo(‘something’)做参数，为Mock对象$observer的update方法配置了被调用时期望的参数’something’

测试过程中，$subject->doSomething()中内部执行$observer的update方法时，如果参数是’something’，update就会以参数something执行，如果不是’something’，对update的测试就标记为失败

例如我们把$this->notify(‘something’);改为$this->notify(‘something2′); 测试就会报告一个失败，测试输出显示如下：

```
PHPUnit_Framework_ExpectationFailedException : Expectation failed for method name is equal to <string:update> when invoked 1 time(s)
Parameter 0 for invocation Observer::update('something2') does not match expected value.
Failed asserting that two strings are equal.
Expected :something
Actual   :something2
```

with()方法可以传入多个期望约束，与mock方法的每一个参数按顺序一一对应：

```php
<?php
class SubjectTest extends PHPUnit_Framework_TestCase
{
    public function testErrorReported()
    {
        // Create a mock for the Observer class, mocking the
        // reportError() method
        $observer = $this->getMock('Observer', array('reportError'));

        $observer->expects($this->once())
                 ->method('reportError')
                 ->with($this->greaterThan(0),//期望reportError的第一个参数大于0
                        $this->stringContains('Something'),//期望reportError的第2个参数含有Something
                        $this->anything());//期望reportError的第一个参数任意

        $subject = new Subject;
        $subject->attach($observer);

        // The doSomethingBad() method should report an error to the observer
        // via the reportError() method
        $subject->doSomethingBad();
    }
}
?>
```


### expects()的参数

在配置mock对象时，expects()方法为mock方法设定期望匹配的次数，即mock方法应该被调用的次数. 通常stub对象配置为$this->any()

##### 匹配方法一共有六种

* any() 不限
* never() 0次
* alLeastOnce() 至少一次，多则不限
* once() 1次
* exactly(int $count) 明确的次数
* at(int $index)  为mock方法设定期望的执行次序位置,例如设为0表示期望这个方法在mock对象的方法中第一个被执行

### wtih()中的约束方法


* anything()	不限类型
* identicalTo(mixed $value)	约束为严格相等

* * * 

* equalTo($value,$delta = 0,$maxDepth = 10) 约束为指定的值(可设定误差和递归深度)
* greaterThan(mixed $value)
* greaterThanOrEqual(mixed $value)
* lessThan(mixed $value)
* lessThanOrEqual(mixed $value)

* * * 

* matches($format) vprintf的格式
* matchesRegularExpression(string $pattern)
* stringContains(string $string, bool $case)
* stringEndsWith(string $suffix)
* stringStartsWith(string $prefix)

* * * 

* isFalse() 约束严格等于false
* isNull() 约束严格等于null
* isTrue() 约束严格等于true
* isEmpty() 约束为空值
* isType(string $type) 约束为某一类型：numeric,scalar,string,int,integer,float,boolean,bool,array,object,null,resource,callable

* * * 

* arrayHasKey(mixed $key)约束数组有指定的key
* contains(mixed $value)约束数组或迭代器含有$value元素
* containsOnly( $type )约束数组或迭代对象的元素全部是$type类型

* * * 

* classHasAttribute(string $attributeName)约束类定义了某个属性
* classHasStaticAttribute(string $attributeName)约束类定义了某个静态属性
* objectHasAttribute( $attributeName )约束对象里面含有某个属性
* attribute(PHPUnit_Framework_Constraint $constraint, $attributeName)
* attributeEqualTo($attributeName, $value, $delta = 0, $maxDepth = 10)约束对象或类的属性的值为$value
* isInstanceOf(string $className)约束对象是某个类或接口的实例

* * * 

* fileExists()约束文件存在
* logicalAnd()约束通过了所有子约束(该方法可以传入多个PHPUnit_Framework_Constraint 对象作为子约束)
* logicalOr()约束通过参数中的至少一个子约束(该方法可以传入多个PHPUnit_Framework_Constraint 对象作为子约束)
* logicalXor()约束只通过参数中的一个子约束(该方法可以传入多个PHPUnit_Framework_Constraint 对象作为子约束)
* logicalNot(PHPUnit_Framework_Constraint $constraint)约束没有通过参数中的子约束


总结：
=======

mock涉及到的参数和方法比较多

{r:final, private ,static methods 不能被stub或mock}

excepts()的参数需要记忆

需要记住will()方法的参数是$this对象的5个return*方法，或throwException(),onConsecutiveCalls()

with()方法的参数则是各种约束方法