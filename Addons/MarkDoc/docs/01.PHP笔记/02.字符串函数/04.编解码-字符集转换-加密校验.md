# 字符编码

## urlencode urldecode

除了可以编码url意外，也可以配合json_encode实现unicode字符不被编码

## base64_encode base64_decode

使用 base64 对 data 进行编码。设计此种编码是为了使二进制数据可以通过非纯 8-bit 的传输层传输，例如电子邮件的主体。

Base64-encoded 数据要比原始数据多占用 33% 左右的空间。

主要用来将二进制文件编码data格式的文件数据

```php
    function base64_encode_image ($imagefile) {
        $imgtype = array('jpg', 'gif', 'png');
        $filename = file_exists($imagefile) ? htmlentities($imagefile) : die('Image file name does not exist');
        $filetype = pathinfo($filename, PATHINFO_EXTENSION);
        if (in_array($filetype, $imgtype)){
            $imgbinary = fread(fopen($filename, "rb"), filesize($filename));
        } else {
            die ('Invalid image type, jpg, gif, and png is only allowed');
        }
        return 'data:image/' . $filetype . ';base64,' . base64_encode($imgbinary);
    }
//<img src="data:image/jpeg;base64,VGhpcyBpcyBhbiBlbmNvZGVkIHN0cmluZw==">
```


失败则返回 FALSE。
 
## convert_uuencode  convert_uudecode

与base64编解码类似，uuencode 算法也会将所有（含二进制）字符串转化为可输出的字符， 并且可以被安全的应用于网络传输。

使用 uuencode 编码后的数据 将会比源数据大35%左右，速度上比base64算法略慢。


## ASCII字符转换

<p class="api">
int ord ( string $string )
</p>

返回字符串 string {r:第一个字节}对应的10进制数值。主要用来返回ascii字符的整形值。


<p class="api">
string chr ( int $ascii )
</p>

根据一个字节所对应的整型数值，返回这个字节的符号 

$ascii 参数可以使用十进制十六进制或8进制的整型，但只能表示一个字节；如需要返回多字节字符，需要连接多次调用,例如：

```php
//utf-8
echo chr(0xe4).chr(0xbd).chr(0xa0);
//输出：你
```

# 加密校验

<p class="api">
int crc32 ( string $str )
</p>

crc32算法单项加密，返回整形数据

<p class="api">
string crypt ( string $str [, string $salt ] )
</p>

返回一个基于标准 UNIX DES 算法或系统上其他可用的替代算法的散列字符串。

从 PHP 5.3.0 起，PHP 包含了它自己的实现，并将在系统缺乏相应算法支持的时候使用它自己的实现。

$salt 可选的盐值字符串。如果没有提供，算法行为将由不同的算法实现决定，并可能导致不可预料的结束。

返回散列后的字符串或一个少于 13 字符的字符串，从而保证在失败时与盐值区分开来。

* 根据以下常量，设置适当长度的盐值字符串：

	* CRYPT_STD_DES - 基于标准 DES 算法的散列使用 "./0-9A-Za-z" 字符中的两个字符作为盐值。在盐值中使用非法的字符将导致 crypt() 失败。

	* CRYPT_EXT_DES - 扩展的基于 DES 算法的散列。其盐值为 9 个字符的字符串，由 1 个下划线后面跟着 4 字节循环次数和 4 字节盐值组成。它们被编码成可打印字符，每个字符 6 位，有效位最少的优先。0 到 63 被编码为 "./0-9A-Za-z"。在盐值中使用非法的字符将导致 crypt() 失败。

	* CRYPT_MD5 - MD5 散列使用一个以 $1$ 开始的 12 字符的字符串盐值。

	* CRYPT_BLOWFISH - Blowfish 算法使用如下盐值：“$2a$”，一个两位 cost 参数，“$” 以及 64 位由 “./0-9A-Za-z” 中的字符组合而成的字符串。在盐值中使用此范围之外的字符将导致 crypt() 返回一个空字符串。两位 cost 参数是循环次数以 2 为底的对数，它的范围是 04-31，超出这个范围将导致 crypt() 失败。

	* CRYPT_SHA256 - SHA-256 算法使用一个以 $5$ 开头的 16 字符字符串盐值进行散列。如果盐值字符串以 “rounds=<N>$” 开头，N 的数字值将被用来指定散列循环的执行次数，这点很像 Blowfish 算法的 cost 参数。默认的循环次数是 5000，最小是 1000，最大是 999,999,999。超出这个范围的 N 将会被转换为最接近的值。

	* CRYPT_SHA512 - SHA-512 算法使用一个以 $6$ 开头的 16 字符字符串盐值进行散列。如果盐值字符串以 “rounds=<N>$” 开头，N 的数字值将被用来指定散列循环的执行次数，这点很像 Blowfish 算法的 cost 参数。默认的循环次数是 5000，最小是 1000，最大是 999,999,999。超出这个范围的 N 将会被转换为最接近的值。

<p class="api">
string md5 ( string $str [, bool $raw_output = false ] )
</p>

raw_output 设为true返回的是16字节的二进制报文。

<p class="api">
string md5_file ( string $filename [, bool $raw_output = false ] )
</p>
<p class="api">
string sha1 ( string $str [, bool $raw_output = false ] )
</p>

<p class="api">
string sha1_file ( string $filename [, bool $raw_output = false ] )
</p>

# 字符集转换

<p class="api">
string utf8_decode ( string $data )
</p>

该函数将用 UTF-8 编码的数据解码为 ISO-8859-1 编码。

<p class="api">
string utf8_encode ( string $data )
</p>

该函数将 ISO-8859-1 编码data 字符串转换为 UTF-8 编码，并返回编码后的字符串

<p class="api">
string iconv ( string $in_charset , string $out_charset , string $str )
</p>

将字符串$str的编码从原始字符集$in_charset,转为目标字符集字符集$out_charset，

默认情况，如果{r:字符串里面含有目标字符集不支持的字符，返回false并发出Notice级别的错误信息，out_charset可以用修饰词改变这个行为}

```php
iconv("UTF-8", "ISO-8859-1//TRANSLIT", $text) //遇到目标字符集中不支持的字符，转换为该字符的英文释义
iconv("UTF-8", "ISO-8859-1//IGNORE", $text)//遇到目标字符集中不支持的字符，转换为该字符的英文释义
```
<p class="api">
bool   mb_check_encoding ([ string $var = NULL [, string $encoding = mb_internal_encoding() ]] )
</p>

检测一个字符串是否是某种编码

如果忽略参数，函数检测http请求中的字符串编码

<p class="api">
string mb_detect_encoding ( string $str [, mixed $encoding_list = mb_detect_order() [, bool $strict = false ]] )
</p>

与mb_check_encoding不同的是，该函数第二个参数可以用数组保存一个字符集列表来检测$str，返回检测字符集结果或false

<p class="api">
string mb_convert_encoding ( string $str , string $to_encoding [, mixed $from_encoding ] )
</p>

用来转换一个字符串的编码

<p class="api">
string mb_convert_variables ( string $to_encoding , mixed $from_encoding , mixed &$vars [, mixed &$... ] )
</p>

$vars可以接受{g:字符串、数组、对象}，函数可以把字符串、数组元素和对象属性的字符转码


# 字符进制转换

## 2进制字符串与16进制字符串互转换

> 这种转换不是数学函数，是纯粹的字符串函数，参数和返回值都是字符串。

<p class="api">
string bin2hex ( string $str )
</p>


<p class="api">
string hex2bin ( string $str )
</p>

该函数需要PHP5.4

```php
echo bin2hex( '你好' );
//输出：e4bda0e5a5bd

$hex = hex2bin("6578616d706c65206865782064617461");
var_dump($hex);
//string(16) "example hex data"
```

## pack和unpack

### 2进制与16进制字符串转换


```php
$binary_string = pack("H*" , 'e4bda0e5a5bd');
//注意：不能使用0x16进制的整型number,number会被转换成10进制整型，然后类型转换为string型，然后这个string被当作16进制字符串。
echo $binary_string;
//utf-8显示为：你好

$hexstr=unpack('H*','你好');//utf-8
输出：e4bda0e5a5bd
```

###  2进制字符串与10进制数值转换

```php
var_dump( unpack( 'C*', '你' ) );
/*输出：
array(3) {  [1] =>  int(228)  [2] =>  int(189)  [3] =>  int(160)}
*/
$arr = pack( 'C*', 228,189,160 );
echo $arr ; //输出:你
```

### utf8转unicode

```php
$cnStr = "中"; //utf8的中文

$code = unpack("H6codes", $cnStr);
//返回数组：Array ( [codes] => e4b8ad )

$cnStr = pack("H6", $code['codes']);
//返回：中

```
