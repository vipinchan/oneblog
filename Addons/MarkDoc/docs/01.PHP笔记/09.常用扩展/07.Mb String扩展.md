安装
----

编译时加上 **--enable-mbstring** 

主要支持的字符集编码：**UTF-8**,**EUC-CN (gb2312)**，**CP936（GBK）**,EUC-TW，big-5，ASCII，HTML-ENTITIES,php5.4增加对GB18030的支持 

配置
------------

如果是独立服务器，可以在php.ini中进行全局的配置

否则可使用mb_函数或ini_set()进行运行时配置

```ini
[mbstring]
;多字节字符串模块支持

mbstring.language = "neutral"
; 默认的NLS(本地语言设置)，可设置值如下：
; 默认值"neutral"表示中立，相当于未知。
; "zh-cn"或"Simplified Chinese"表示简体中文
; "zh-tw"或"Traditional Chinese"表示繁体中文
; "uni"或"universal"表示Unicode
; 该指令自动定义了随后的mbstring.internal_encoding指令默认值，
; 并且mbstring.internal_encoding指令必须放置在该指令之后。

mbstring.internal_encoding =
; 本指令必须放置在mbstring.language指令之后。
; 默认的内部编码，即mb函数的编码参数的默认值。未设置时取决于mbstring.language指令的值：
; "neutral" 对应 "ISO-8859-1"
; "zh-cn"   对应 "EUC-CN" ("GB2312"的别名)
; "zh-tw"   对应 "EUC-TW" (等价于"BIG5")
; "uni"     对应 "UTF-8"
; 提醒：对于简体中文还可以强制设置为"CP936" (等价于"GBK")
; 注意：可能 SJIS, BIG5, GBK 不适合作为内部编码，不过"GB2312"肯定没问题。
; 建议手动强制指定

mbstring.encoding_translation = Off
; 是否对进入的HTTP请求按照mbstring.internal_encoding指令进行透明的编码转换，
; 也就是自动检测输入字符的编码并将其透明的转化为内部编码。
; 可移植的库或者程序千万不要依赖于自动编码转换。

mbstring.http_input = "pass"
; 默认的HTTP输入编码，"pass"表示跳过(不做转换)
; "aotu"的含义与mbstring.detect_order指令中的解释一样。
; 可以设置为一个单独的值，也可以设置为一个逗号分隔的列表。

mbstring.http_output = "pass"
; 默认的HTTP输出编码，"pass"表示跳过(不做转换)
; "auto"的含义与mbstring.detect_order指令中的解释一样。
; 可以设置为一个单独的值，也可以设置为一个逗号分隔的列表。
; 必须将output_handler指令设置为"mb_output_handler"才可以。

mbstring.detect_order =
; 默认的编码检测顺序，"pass"表示跳过(不做转换)。
; 默认值("auto")随mbstring.language指令的不同而变化：
; "neutral"和"universal" 对应 "ASCII, UTF-8"
; "Simplified Chinese"   对应 "ASCII, UTF-8, EUC-CN, CP936"
; "Traditional Chinese"  对应 "ASCII, UTF-8, EUC-TW, BIG-5"
; 建议在可控环境下手动强制指定为："UTF-8,EUC-CN,CP936,ASCII"

mbstring.func_overload = 0
; 自动使用 mb_* 函数重载相应的单字节字符串函数。建议值1，根据实际情况直接调用mb_*方法

mbstring.script_encoding =
; 脚本所使用的编码

mbstring.strict_detection = Off
; 是否使用严谨的编码检测

mbstring.substitute_character =
; 当某个字符无法解码时，就是用这个字符替代。
; 若设为一个整数则表示对应的Unicode值，不设置任何值表示不显示这个错误字符。
; 建议设为"□" 
```

函数
==========

> 对于有字符串选择功能的函数，应使用mb函数，避免截取多字节字符串出现问题。
>
> 对于有字符串搜索功能的函数，如果是gbk,gb18030等编码，应使用mb函数；如果是utf-8或者gb2312字符集，则可以安全使用内置函数。


常用函数
-----------

> mb扩展可以按字节或字符选择和搜索字符串而避免出现问题

<p class="api">string mb_strcut(string $str,int $start[,int $length[,string $encoding]])</p>

按字节截取字符串传，如果最后一个位置位于多字节字符的中间，则将终点移动到这个字符之前。

<p class="api">string mb_substr(string $str,int $start[,int $length[,string $encoding]])</p>

按字符数量截取字符串

<p class="api">int mb_strlen(string $str[,string $encoding])</p>

统计字符数量

<p class="api">int mb_strwidth(string $str[,string $encoding])</p>

计算字符串的屏幕显示宽度，大多多字节字符的显示宽度都是两个单字节字符的宽度。

```php
$str ="你好";
echo mb_strwidth( $str,'utf-8' );//4
echo strlen( $str );//6
echo mb_strlen( $str,'utf-8' );//2
```

<p class="api">int mb_substr_count(string $haystack,string $needle[,string $encoding])</p>

子字符串数量

<p class="api">string mb_strimwidth(string $str,int $start,int $width[,string $trimmarker[,string $encoding]])</p>

截取指定显示宽度(不是字符数，也不是字节数子结束自己不是)的字符串

$start是偏移的字符数量，$width是截取的显示宽度，$trimmarker是截取后的后缀常用“...”，后缀宽度也计入width

如果截取位置位于一个汉字的中间，则返回这个汉字之前的部分，汉字不会被按字节截断

位置测定：
-----------

> offset是相对字符串头部偏移的{g:字符数量}

<p class="api">int mb_strpos(string $haystack,string $needle[,int $offset=0[,string $encoding]])</p>

<p class="api">int mb_strrpos(string $haystack,string $needle[,int $offset=0[,string $encoding]])</p>

<p class="api">int mb_stripos(string $haystack,string $needle[,int $offset=0[,string $encoding]])</p>

<p class="api">int mb_strripos(string $haystack,string $needle[,int $offset=0[,string $encoding]])</p>

<p class="api">array mb_split(string $pattern,string $string[,int $limit=-1])</p>


编码转换
------------

<p class="api">bool   mb_check_encoding([string $var=NULL[,string $encoding=mb_internal_encoding()]])</p>

这个函数可以检测一个字符串是不是某种字符集。

很多汉字在gb2312,cp936,big-5中使用相同的编码，这类汉字用这些字符集检测都可以返回true

<p class="api">string mb_detect_encoding(string $str[,mixed $encoding_list=mb_detect_order()[,bool $strict=false]])</p>

这个函数可以从一个字符集列表中，侦测字符串的字符集。建议手动指定可能的字符集，默认只有ASCII和UTF-8

列表可以是这些字符集中的任意个："UTF-8,EUC-CN,CP936,BIG-5,EUC-TW,ASCII,"

<p class="api">string mb_convert_encoding(string $str,string $to_encoding[,mixed $from_encoding])</p>

转换字符串的字符集后返回转换以后的字符串，原字符串保持不变。

例如：`mb_convert_encoding('&#x25B6;', 'UTF-8', 'HTML-ENTITIES')`

<p class="api">string mb_convert_variables(string $to_encoding,mixed $from_encoding,mixed &$vars[,mixed &$...])</p>

直接对变量内的字符串进行转码，改变了原变量，可以一次转换多个变量。

$vars也也可以是数组，对象，它将对数组的元素的value，对象属性的value执行转换(不对key进行转换，因为key是变量名的一部分)

按字符截取
--------------

<p class="api">string mb_strstr(string $haystack,string $needle[,bool $before_needle=false[,string $encoding]])</p>

<p class="api">string mb_stristr(string $haystack,string $needle[,bool $before_needle=false[,string $encoding]])</p>

下面两个函数比内置函数多了一个before_needle功能

<p class="api">string mb_strrchr(string $haystack,string $needle[,bool $before_needle=false[,string $encoding]])</p>

<p class="api">string mb_strrichr(string $haystack,string $needle[,bool $before_needle=false[,string $encoding]])</p>

大小写转换
-----------

可以对俄文德文等拼音语言转换大小写，而不仅仅是英语。对中文应用没有意义。

<p class="api">string mb_strtoupper(string $str[,string $encoding=mb_internal_encoding()])</p>

<p class="api">string mb_strtolower(string $str[,string $encoding=mb_internal_encoding()])</p>

<p class="api">string mb_convert_case(string $str,int $mode=MB_CASE_UPPER[,string $encoding=mb_internal_encoding()])</p>

MB_CASE_UPPER, MB_CASE_LOWER,

MB_CASE_TITLE：将单词首字符大写，同时将单词其他字符小写(与ucwords()相同)


配置函数
-----------

<p class="api">mixed mb_substitute_character([mixed $substrchar])</p>

<p class="api">mixed mb_language([string $language])</p>

<p class="api">mixed mb_internal_encoding([string $encoding=mb_internal_encoding()])</p>
