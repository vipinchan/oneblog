
php.ini配置
============

Profiles分析工具
-----------------

Linux:Kcachegrind

win:Wincachegrind

web:Webgrind

第一部分：基本配置:
------------------

```ini
xdebug.default_enable
类型：布尔型 默认值：On
如果这项设置为On，堆栈跟踪将被默认的显示在错误事件中。你可以通过在代码中使用xdebug_disable()来禁止堆叠跟踪的显示。
因为这是xdebug基本功能之一，将这项参数设置为On是比较明智的。

xdebug.max_nesting_level
类型：整型 默认值：100
The value of this setting is the maximum level of nested functions that are allowed before the script will be aborted.
限制无限递归的访问深度。这项参数设置的值是脚本失败前所允许的嵌套程序的最大访问深度。

xdebug.scream
类型：布尔型。默认值：off
如果设为on，则代码中的@错误抑制符合将被禁用，>=2.1

xdebug.overload_var_dump
默认值为1
var_dump重载：xdebug重载了系统的var_dump函数，输出的变量信息格式可通过php.ini控制

xdebug.var_display_max_depth, 设置递归显示时嵌套的深度，默认值为3
xdebug.var_display_max_data，设置字符串显示的最长长度，值为-1不进行限制
xdebug.var_display_max_children，设置递归显示出来的总的子元素数量，值为-1不进行限制
xdebug.cli_color，设置cli模式下是否显示颜色。默认为0，值为1时，在tty中输出时显示颜色，设为2则总是显示颜色
```

第二部分：Stack Traces
-----------------------

```ini
xdebug.dump_globals
类型：布尔型 默认值：1
设置是否追踪超全局变量
例如，xdebug.dump.SERVER = REQUEST_METHOD,REQUEST_URI,HTTP_USER_AGENT 将打印 PHP 超全局变量
$_SERVER['REQUEST_METHOD']、$_SERVER['REQUEST_URI'] 和 $_SERVER['HTTP_USER_AGENT']。

xdebug.dump_once
类型：布尔型 默认值：1
设置是否超全局变量的在所有出错环境下每次都dump。设置为Off时仅在开始的地方dump一次

xdebug.dump_undefined
类型：布尔型 默认值：0
如果你想dump超全局变量中未定义的值，你应该把这个参数设置成On，否则就设置成Off

xdebug.show_exception_trace
类型：整型 默认值：0
当这个参数被设置为1时，即使捕捉到异常，xdebug仍将强制执行异常跟踪。

xdebug.show_local_vars
类型：整型 默认值：0
当这个参数被设置为不等于0时，xdebug在错环境中所产生的堆栈转储还将显示所有局部变量，
包括尚未初始化的变量在最上面。要注意的是这将产生大量的信息，也因此默认情况下是关闭的。

xdebug.show_mem_delta
整型：当设为非0值时，会显示函数调用前后的内存对比

xdebug.collect_assignments
类型: boolean, 默认: 0
controls whether Xdebug should add variable assignments to function traces.

xdebug.collect_includes
类型: boolean, 默认: 1
controls whether Xdebug should write the filename used in include(), include_once(), require() or require_once() to the trace files.

xdebug.collect_params，默认值为0，不显示函数的参数信息
设为1，可显示错误发生时函数的参数的类型和元素数量，例如 string(6), array(8)
设为2，同上，但提供了tip tool，显示参数完整信息
设为3，可显示错误发生时函数的各个参数的值
设为4，可显示错误发生时函数的参数变量名和值

xdebug.collect_vars
布尔值：默认为0。打开此项会比较慢，它会输出作用域内的变量详情
```

第三部分：profiler
-------------------

```ini
xdebug.profiler_append
类型：整型 默认值：0
当这个参数被设置为1时，如果文件名相同，以追加模式写入文件

xdebug.profiler_enable
类型：整型 默认值：0
设为1时，将会自动生成profiler文件。如果设为0，则可把 xdebug.profiler_enable_trigger 设为1,通过cookie控制profiler文件生成.{r: 这个设置不能通过在你的脚本中调用ini_set()来设置。}

xdebug.profiler_enable_trigger
Type: integer, Default value: 0
默认为0，不会每次都生成profiler文件；设为1则可以通过get或post或cookie来发送 XDEBUG_PROFILE 变量触发profiler创建

xdebug.profiler_output_dir
类型：字符串 默认值：/tmp
这个文件是profiler文件输出目录。{r:这个设置不能通过在你的脚本中调用ini_set()来设置。}

xdebug.profiler_output_name
类型：字符串 默认值：cachegrind.out%p
```

第四部分：IDE调试
------------------

```php
xdebug.remote_autostart
类型：布尔型 默认值：0
一般来说，你需要使用明确的HTTP GET/POST变量来开启远程debug。而当这个参数设置为On，xdebug将经常试图去开启一个远程debug session并试图去连接客户端，即使GET/POST/COOKIE变量不是当前的。

xdebug.remote_enable
类型：布尔型 默认值：0
设为1，开启IDE远程调试功能

xdebug.remote_host
类型：字符串 默认值：localhost
选择debug客户端正在运行的主机（即运行IDE电脑的ip）

xdebug.remote_port
类型：整型 默认值：9000
这个端口是xdebug试着去连接远程主机的。9000是一般客户端和被绑定的debug客户端默认的端口。许多客户端都使用这个端口数字，最好不要去修改这个设置。

xdebug.remote_connect_back
Type: boolean, Default value: 0
当设为1，romote_host不再生效,xdebug根据request信息中的ip来获取客户断地址

xdebug.remote_cookie_expire_time

xdebug的cookie生命周期，默认为：3600
```

浏览器访问方式

url加入一个get变量XDEBUG_SESSION_START=name激活debug模式

url加入一个get变量XDEBUG_SESSION_STOP停止debug模式

第五部分：trace分析
--------------------

```php
xdebug.auto_trace 
类型：boolean  默认值：0
是否自动记录PHP函数调用跟踪信息，如果设置为1，则xdebug会记录PHP程序的函数调用信息，

xdebug.trace_output_dir 
类型：string  默认：/tmp
trace文件保持位置

xdebug.trace_output_name 
类型：string  默认:trace.%c
trace文件命名格式

xdebug.trace_enable_trigger 
类型：boolean  默认：0
当设为1时，并且xdebug.auto_trace=0  可以使用GET/POST方式发送 XDEBUG_TRACE 或者设置为cookie名来触发trace

xdebug.trace_format 
类型：int   默认：0
该值控制trace信息的格式，有0，1，2三个值供选择，其中
-  0 ：展示易读的trace信息，信息内容包括，开销时间，内存使用，memory delta信息,
等级，函数名，函数参数（如果xdebug.collect_params功能是打开的），文件名以及行号
- 1：产生计算机可识别的trace信息
- 2：产生html形式的trace信息

xdebug.collect_return
类型:布尔   默认:off
是否在trace文件中写入函数返回值

xdebug.trace_options
Type: integer, Default value: 0
设为1，则以追加模式写trace文件。默认是以覆盖模式写入

xdebug.coverage_enable
设为1，开启代码覆盖率功能。Xdebug > 2.2
```


### trace和profile文件命名支持的format

|Specifier|Meaning                               |Example Format   |Example Filename                                    |
|---------|--------------------------------------|-----------------|----------------------------------------------------|
|%c       |当前工作目录的 crc32编码              |trace.%c         |trace.1258863198.xt                                 |
|%p       |pid                                   |trace.%p         |trace.5174.xt                                       |
|%r       |random number                         |trace.%r         |trace.072db0.xt                                     |
|%s       |script name (只对profile有效)         |cachegrind.out.%s|cachegrind.out._home_httpd_html_test_xdebug_test_php|
|%t       |timestamp (seconds)                   |trace.%t         |trace.1179434742.xt                                 |
|%u       |timestamp (microseconds)              |trace.%u         |trace.1179434749_642382.xt                          |
|%H       |$_SERVER['HTTP_HOST']                 |trace.%H         |trace.kossu.xt                                      |
|%R       |$_SERVER['REQUEST_URI']               |trace.%R         |trace._test_xdebug_test_php_var=1_var2=2.xt         |
|%U       |$_SERVER['UNIQUE_ID'] (v2.2 需要apache的mod_unique_id module)|trace.%U         |trace.TRX4n38AAAEAAB9gBFkAAAAB.xt                   |
|%S       |session_id (from $_COOKIE if set)     |trace.%S         |trace.c70c1ec2375af58f74b390bbdd2a679d.xt           |
|%%       |literal %                             |trace.%%         |trace.%%.xt                                         |




函数
======

基本函数
--------

<p class="api">string xdebug_call_class()</p>

Returns the calling class

<p class="api">string xdebug_call_file()</p>

Returns the calling file

<p class="api">string xdebug_call_function()</p>

Returns the calling function/method(返回调用者的函数名或method字符串)

<p class="api">int xdebug_call_line()</p>

Returns the calling line

<p class="api">void xdebug_disable()</p>

Disables stack traces

<p class="api">void xdebug_enable()</p>

Enables stack traces

<p class="api">bool xdebug_is_enabled()</p>

Returns whether stack traces are enabled

<p class="api">array xdebug_get_headers()</p>

>=2.1

返回header、setcookie函数等php内部函数所发出的所有头信息。

<p class="api">int xdebug_memory_usage()</p>

返回当前脚本此时所用的内存。

<p class="api">int xdebug_peak_memory_usage()</p>

返回当前脚本从开始执行至运行到此时所用的内存的最大值。

<p class="api">void xdebug_start_error_collection()</p>

Introduced in version 2.1

开始记录所有的notices, warnings 和 errors 并且阻止它们的输出

<p class="api">void xdebug_stop_error_collection()</p>

开始记录所有的notices, warnings 和 errors。

<p class="api">void xdebug_get_collected_errors([int clean])</p>

用html表格输出收集到的所有错误警告等信息。默认执行后不会清空之前收集到的内容，如果需要清空，参数使用true

<p class="api">float xdebug_time_index()</p>

返回脚本从开始到现在经过的时间

变量显示函数
------

<p class="api">void xdebug_debug_zval([string varname[,...]])</p>

显示一个变量的zval信息，如果是变量数组，还会有数组元素的zval信息。

```php
$a = array('A', 'B', 'C');
$b =& $a;
$c =& $a[2];

xdebug_debug_zval('a');

/*
输出$a以及$a的元素的zval信息:
a: (refcount=2, is_ref=1)=array (
		0 => (refcount=1, is_ref=0)='A', 
		1 => (refcount=1, is_ref=0)='B', 
		2 => (refcount=2, is_ref=1)='C')
*/
```

<p class="api">void xdebug_debug_zval_stdout([string varname[,...]])</p>

这个函数与 xdebug_debug_zval 的不同在于，它只将信息输出到STOUT，而不输出到webserver。所以信息不会在浏览器中显示，只能在命令行窗口显示。


Function Stack Traces
-------------

> 可以将trace信息返回到变量，也可以直接输出，也可以写入到文件中。

<p class="api">array xdebug_get_declared_vars()</p>

dump作用域内所有变量的变量名

<p class="api">array xdebug_get_function_stack()</p>

dump函数的trace信息，放在函数和方法中调用，以数组返运行到该函数的过程的stack信息。

对于调试回调和递归及方法的调用很有用。

返回结果示例：
```php
Array
(
    [0] => Array
        (
            [function] => {main}
            [file] => /home/zhuyajie/Dropbox/wiki/markdown/markdown.php
            [line] => 0
            [params] => Array
                (
                )
        )

    [1] => Array
        (
            [function] => savehtml
            [file] => /home/zhuyajie/Dropbox/wiki/markdown/markdown.php
            [line] => 3004
            [params] => Array
                (
                    [mdpath] => '/home/zhuyajie/Dropbox/wiki/notebook/01.PHP笔记/09.常用扩展/11.Xdebug.md'
                )
        )
)
```

<p class="api">int xdebug_get_stack_depth()</p>

dump当前的Stack深度，主要用于调试递归。

<p class="api">none xdebug_print_function_stack([string $message])</p>

显示运行到该函数的过程的stack信息，结果示例如下：

```
Xdebug:  in /tmp/php1360321730dxUtZg on line 2

Call Stack:
    0.0001     331760   1. {main}() /home/zhuyajie/Dropbox/TPhelper/index.php:0
    0.0004     345128   2. require_once('/home/zhuyajie/Dropbox/ThinkPHP/ThinkPHP/ThinkPHP.php') /home/zhuyajie/Dropbox/TPhelper/index.php:7
    0.0012     440008   3. require('/home/zhuyajie/Dropbox/ThinkPHP/ThinkPHP/Common/runtime.php') /home/zhuyajie/Dropbox/ThinkPHP/ThinkPHP/ThinkPHP.php:31
    0.0038     719308   4. Think::start() /home/zhuyajie/Dropbox/ThinkPHP/ThinkPHP/Common/runtime.php:240
    0.0096    1292700   5. App::run() /home/zhuyajie/Dropbox/ThinkPHP/ThinkPHP/Lib/Core/Think.class.php:39
    0.0122    1438752   6. App::exec() /home/zhuyajie/Dropbox/ThinkPHP/ThinkPHP/Lib/Core/App.class.php:222
    0.0251    2379436   7. ReflectionMethod->invoke(class ToolAction { protected $common_tpl = array ('header' => 'Common:header', 'navbar' => 'Common:navbar', 'footer' => 'Common:footer'); protected $include_tpl = array (); protected $view = NULL; private ${Action}:name = ''; protected $tVar = array ('listapp' => array (...), 'pagetitle' => 'ThinkPHP助手', 'waittime' => 3, 'cookie_prefix' => 'TPhelper_', 'th_version' => '1.1.3'); protected $config = array () }) /home/zhuyajie/Dropbox/ThinkPHP/ThinkPHP/Lib/Core/App.class.php:186
    0.0251    2379452   8. ToolAction->fragmentTest() /home/zhuyajie/Dropbox/ThinkPHP/ThinkPHP/Lib/Core/App.class.php:186
    0.0253    2380520   9. include('/tmp/php1360321730dxUtZg') /home/zhuyajie/Dropbox/TPhelper/Lib/Action/ToolAction.class.php:26
    0.0253    2380592  10. xdebug_print_function_stack('') /tmp/php1360321730dxUtZg:2
```

{g:将以上信息粘贴到 Phpstorm 的 Tools->Analyze Stacktrace... 可以通过点击跳转到相应文件的相应位置}

<p class="api">string xdebug_get_tracefile_name()</p>

返回用来记录trace的文件path,This is useful when xdebug.auto_trace is enabled.

<p class="api">void xdebug_start_trace(string $trace_file[,integer $options])</p>

开始将trace信息写入文件

#####$options参数：

* XDEBUG_TRACE_APPEND (1) 追加模式写入
* XDEBUG_TRACE_COMPUTERIZED (2) 按 "xdebug.trace_format"设置的格式生成
* XDEBUG_TRACE_HTML (4) 生成html格式


<p class="api">void xdebug_stop_trace()</p>

Stop tracing function calls and closes the trace file.


Code Coverage Analysis
------------------------

<p class="api">void xdebug_start_code_coverage([int $options])</p>
This function starts gathering the information for code coverage. 
The information that is collected consists of an two dimensional array with as primary index the executed filename and as secondary key the line number. 
The value in the elements represents the total number of execution units on this line have been executed.

##### $options

* XDEBUG_CC_UNUSED Enables scanning of code to figure out which line has executable code. 
* XDEBUG_CC_DEAD_CODE Enables branch analyzes to figure out whether code can be executed.

<p class="api">void xdebug_stop_code_coverage([int cleanup=true])</p>

Stops code coverage

This function stops collecting information, the information in memory will be destroyed. 
If you pass "false" as argument, then the code coverage information will not be destroyed so that you can resume the gathering of information with the xdebug_start_code_coverage() function again.

<p class="api">array xdebug_get_code_coverage()</p>

Returns code coverage information

Returns a structure which contains information about which lines were executed in your script (including include files).