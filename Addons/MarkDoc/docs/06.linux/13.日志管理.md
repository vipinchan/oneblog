Linux 常见的日志文件
=============

/var/log/cron：计划任务日志

/var/log/dmesg：系统启动过程日志

/var/log/lastlog：最近登录日志

/var/log/maillog 或 /var/log/mail/*：其实主要是记录 sendmail (SMTP 协议提供者) 与 dovecot (POP3 协议提供者) 所产生的信息啦。

/var/log/messages：系统的错误信息 (或者是重要的资讯) 日志

/var/log/secure：基本上，只要牵涉到『需要输入帐号口令』的软件，那么当登陆时 (不管登陆正确或错误) 都会被记录在此文件中。 包括系统的 login 程序、图形介面登陆所使用的 gdm 程序、 su, sudo 等程序、还有网络连线的 ssh, telnet 等程序， 登陆资讯都会被记载在这里；

/var/log/wtmp, /var/log/faillog：这两个文件可以记录正确登陆系统者的帐号资讯 (wtmp) 与错误登陆时所使用的帐号资讯 (faillog)

/var/log/httpd/*, /var/log/news/*, /var/log/samba/*：不同的网络服务会使用它们自己的登录文件来记载它们自己产生的各项信息！上述的目录内则是个别服务所制订的登录文件。
