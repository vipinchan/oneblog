应用执行流程
==========

ThinkPHP.php
-----------

1. 定义各种常量
1. 载入 `Common/runtime.php`


runtime.php
-------------

1. 确定所有必须的常量

1. 载入核心文件和核心行为，生成编译缓存

	1. `Common/common.php`  核心函数文件
	
		文件中定义了 单字母快捷函数 G,D,M,A,L,R,N,C,B,W，类库和函数导入函数，标签和别名处理函数
		
	1. `Lib/Core/Think.class.php`
	
		该类完成app的初始化，和运行工作
	
	1. `Core/ThinkException.class.php` TP的异常类，其实例用于记录app初始化和运行过程中发生的异常
	
	1. `Core/Behavior.class.php`  载入行为抽象基类类
	
	1. `Lib/Behavior/*Behavior.class.php`  载入系统行为类，在app初始化和执行过程中根据行为标签按需触发执行
	
1. **Think::start()**


	* `register_shutdown_function(array('Think','fatalError'));`

	* `set_error_handler(array('Think','appError'));`

	* `set_exception_handler(array('Think','appException'));`

	* `spl_autoload_register(array('Think', 'autoload'));`

	* **Think::buildApp();**
		
		1. 加载底层惯例配置 `THINK_PATH.'Conf/convention.php'`

		1. 读取模式配置 `MODE_PATH.strtolower(MODE_NAME).'.php'`

		1. 加载项目配置 `CONF_PATH.'config.php'`

		1. 加载框架底层语言包

		1. 加载系统内置行为配置 `THINK_PATH.'Conf/tags.php'`

		1. 加载项目行为配置 `CONF_PATH.'tags.php'`

		1. 加载基础应用内核文件

			* `THINK_PATH.'Common/functions.php', // 标准模式函数库`

			* `CORE_PATH.'Core/Log.class.php', // 日志处理类`

			* `CORE_PATH.'Core/Dispatcher.class.php', // URL调度类`

			* `CORE_PATH.'Core/App.class.php', // 应用程序类`

			* `CORE_PATH.'Core/Action.class.php', // 控制器类`

			* `CORE_PATH.'Core/View.class.php', // 视图类`

		1. 加载自定义内核文件 `CONF_PATH.'core.php'`

		1. 加载应用公共函数库文件 `COMMON_PATH.'common.php'`

		1. 加载模式别名定义类库 

		1. 加载应用别名类库 `CONF_PATH.'alias.php'`

		1. 加载应用的调试模式配置文件(APP_DEBUG为true时) `THINK_PATH.'Conf/debug.php'` `CONF_PATH.C('APP_STATUS').'.php'`

	* **App::run();**

		1. `tag('app_init');//空`

		1. `App::init();`

			* 时区设置

			* 载入动态配置 `LOAD_EXT_FILE` 和 `LOAD_EXT_CONFIG` 定义的文件

			* 执行URL分析调度

			* 定义request相关常量：`IS_POST`，`IS_GET` ,`IS_PUT` ,`IS_DELETE` ,`IS_AJAX` ,`NOW_TIME`

			* `tag('url_dispatch'); //空`

			* `C('VAR_FILTERS')`为true时，过滤GET和POST变量

			* 获取模板主题，定义模板主题相关常量

		1. `tag('app_begin');//执行'ReadHtmlCache'行为,读取静态缓存`

		1. `session(C('SESSION_OPTIONS'));// Session初始化`

		1. `G('initTime');// 记录应用初始化时间`

		1. `App::exec();`

			* `__construct()`

				* `tag('action_begin',$this->config);`

				* `$this->_initialize()`

			* `__destruct()`

				* `tag('action_end');`

		1. `tag('app_end');//空`

常量定义
==========

入口文件
----------

在入口文件中，可以定义的常量有：**THINK_PATH**，**APP_DEBUG**，**APP_NAME**，**APP_PATH**。

通常 **THINK_PATH**是必须定义的，其他三个可以不定义，TP会设置默认值。

**APP_DEBUG** 默认为false，可以不定义

**APP_PATH** 默认为 `dirname($_SERVER['SCRIPT_FILENAME'])`的目录，这意味着默认值为入口文件所在目录;
对于Topthink多应用引擎，APP_PATH是根据访问的具体应用动态定义的，无需手动定义：`define('APP_PATH', APPS_PATH.APP_NAME.'/');`

**APP_NAME** 默认为 `basename(dirname($_SERVER['SCRIPT_FILENAME']))`