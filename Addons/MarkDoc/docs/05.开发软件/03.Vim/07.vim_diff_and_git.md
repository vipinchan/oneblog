vimdiff
==========

启动时执行比较
--------------

$ vimdiff file1 file2 [file3 ...]
$ vim -d file1 file2  [file3 ...]
$ gvimdiff file1 file2 [file3 ...]
$ vim -d -g file1 file2  [file3 ...]

也可以使用 **viewdiff**, **gviewdiff**只读方式比较文件

编辑时执行比较
-------------------

	:diffs[plit] main.c~    #垂直分隔窗口与另一个文件执行比较
	:vert[ical] diffsplit main.c~ # 水平分隔窗口与另一个文件执行比较
    :difft[this]   #把当前窗口设为diff窗口,与其他处于diff状态的窗口进行比较
    :diffo[ff]     #关闭当前窗口的diff状态


	:set noscrollbind

跳至changes
----------

	]c :下一处change
	[c :上一处change

diffupdate
-----------

在diff窗口修改文件后,更新diff高亮


Diff修改
----------

光标移动到有改变的行执行以下命令:

    :[range]diffpu[t] [bufspec],把范围内的改变复制到另一边的窗口

    dp  把光标处的行复制到另一边的窗口(没有任何参数和范围为当前行的diffput)

    :[range]diffg[et] [bufspec]

    do  从另一边的窗口获取change,替换当前窗口中的change(没有任何参数和范围为当前行的diffget)

[bufspec] 参数可以是buffer number, 正则表达式, buffer name的一部分.  Examples:

	:diffget		Use the other buffer which is in diff mode
	:diffget 3		Use buffer 3
	:diffget v2		Use the buffer which matches "v2" and is in diff mode (e.g., "file.c.v2")

fugitive
==========


