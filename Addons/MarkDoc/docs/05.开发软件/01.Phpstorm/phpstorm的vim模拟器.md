在vim模拟器无效的vim命令
-----------

一般模式：`g~~`

编辑模式：`ctrl+p`

有效设置 
----------

* :set autoindent
* :set ignorecase
* :set hlsearch
* :nohlsearch
* :history
* :marks
* :jumps

功能支持情况
------------

* 支持vim的宏命令，支持vim的正则和替换
* 不支持在命令模式执行shell命令
* 不支持命令模式的自动完成
* 不支持vim的格式化功能

vim模拟器使用的热键
------------------

一般模式
: `ctrl+v,ctrl+u,ctrl+d,ctrl+f,ctrl+b,ctrl+e,ctrl+y,ctrl+r,ctrl+o,ctrl+i`

编辑模式
: `ctrl+b,ctrl+u,ctrl+w,ctrl+c,ctrl+d,ctrl+t,ctrl+a,ctrl+y,ctrl+e,ctrl+m,ctrl+h`

与默认keymap不同的热键
------------------

* 复制到剪贴板:`ctrl+insert`
* 从剪贴板粘贴:`shift+insert`
* 剪切到剪贴板:`shift+delete`
* 函数参数提示：`ctrl+p=>ctrl+shift+p`
* 类的层次结构: `ctrl+h =>ctrl + alt +shift + H(同时激活ctrl+alt+UP/DOWN快捷键)`
* Go to Class: `ctrl+N => alt + shift +N`
* Go to super-method/super-class: `ctrl+u => ctrl+ shift +U`
* 文档查看: `ctrl+Q => ctrl + alt + Q`


phpstrom快捷键
------------

* `ctrl+alt+L` 代码格式化
* `ctrl+alt+i` 自动缩进到合理位置
* `alt+enter`   Inspection菜单
* `alt+insert`  自动生成功能
* `alt+home` 到导航条
* `ctrl + alt +B`  跳至光标处变量\函数\类的定义位置
* `Ctrl + Alt + Shift + N` 跳至指定的变量\常量\方法
* `F2,shift+F2` 下一处/上一处错误
* `ctrl+alt+H` 显示光标处方法被调用的详情(同时激活ctrl+alt+UP/DOWN快捷键)
* `ctrl+shift+H` 方法的层次结构(同时激活ctrl+alt+UP/DOWN快捷键)
* `ctrl+alt+T`   Surround with
* `ctrl+alt+J`   Surround with Live template
* `ctrl+shift+UP/DOWN`  上下移动当前行
* `alt+shift+UP/DOWN`  上下移动选择的块行(V模式)
* `shift+F6`  rename
* `ctrl+alt+a`  git add
* `ctrl+K` git commit
* `ctrl+shift+delete` xml类代码剥去标签
