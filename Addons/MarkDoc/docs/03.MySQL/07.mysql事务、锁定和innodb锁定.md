事务
======

事务不能被嵌套

start transaction | begin [work]
---------

可以开始一项新的事务,临时禁用autocommit,直到直到成功执行完commit才恢复autocommit

{r:开始一项事务隐含着UNLOCK TABLES被执行}

commit [work] [and [no] chain] [[no] release]
----------

提交当前事务，使变更成为永久变更

and chain子句会在当前事务结束时，立刻启动一个与刚结束的事务有相同的隔离等级的新事务。

release 子句在终止了当前事务后，会让服务器断开与当前客户端的连接。

包含no关键词可以抑制chain或release完成。如果completion_type系统变量被设置为一定的值，使连锁或释放完成可以默认进行，此时no关键词有用。默认情况下，mysql采用autocommit模式运行。这意味着，当您执行一个用于更新（修改）表的语句之后，mysql立刻把更新存储到磁盘中。

rollback [work] [and [no] chain] [[no] release]
------------

回滚当前事务，取消其变更。有些语句不能被回滚。比如创建或取消数据库的语句，和创建、取消或更改表或存储的子程序的语句。如果您在事务的前部中发布了一个不能被回滚的语句，则后部的其它语句会发生错误。

set autocommit = {0 | 1}
----------

可以禁用或启用默认的autocommit模式，用于当前连接,如果禁用了，则每次执行sql语句都要手动commit;

对于InnoDB表，如果要使用锁定，您不应该让AUTOCOMMIT=1，如果AUTOCOMMIT=1，我们根本不能获取InnoDB表锁定。



会造成隐式提交的语句
-------

以下语句（以及同义词）在执行前，会隐形地先执行一个COMMIT：

**ALTER FUNCTION, ALTER PROCEDURE, ALTER TABLE, BEGIN, CREATE DATABASE, CREATE FUNCTION, CREATE INDEX, CREATE PROCEDURE, CREATE TABLE, DROP DATABASE, DROP FUNCTION, DROP INDEX, DROP PROCEDURE, DROP TABLE, LOAD MASTER DATA, LOCK TABLES, RENAME TABLE, SET AUTOCOMMIT=1, START TRANSACTION, TRUNCATE TABLE, UNLOCK TABLES**


SAVEPOINT 和 ROLLBACK TO SAVEPOINT
---------------------

### SAVEPOINT identifier

用于设置一个事务保存点，带一个标识符名称。如果当前事务有一个同样名称的保存点，则旧的保存点被删除，新的保存点被设置。

### ROLLBACK [WORK] TO SAVEPOINT identifier

向已命名的保存点回滚一个事务。如果在保存点被设置后，当前事务对行进行了更改，则这些更改会在回滚中被撤销。但是，InnoDB不会释放被存储在保存点之后的存储器中的行锁定。在被命名的保存点之后设置的保存点被删除。如果语句返回以下错误，则意味着不存在带有指定名称的保存点：ERROR 1181: Got error 153 during ROLLBACK

### RELEASE SAVEPOINT identifier

从当前事务的一组保存点中删除已命名的保存点。不出现提交或 回滚。如果保存点不存在，会出现错误。

LOCK TABLES和UNLOCK TABLES语法
-----------------------------

	lock tables  tbl_name [as alias] {read [local] | [low_priority] write}
					[, tbl_name [as alias] {read [local] | [low_priority] write}] ...

	UNLOCK TABLES

locak tables 可以锁定用于当前线程的表。如果表被其它线程锁定，则造成堵塞，直到可以获取所有锁定为止。

当您调用LOCK TABLES时，InnoDB会内部地取其自己的表锁定，然后在InnoDB在下一个提交时释放innodb表锁定，假如AUTOCOMMIT=1，执行lock后立即会有一个隐含的commit被提交，提交之后innodb马上释放其表锁定，导致根本无法获得innodb表锁定，而且容易造成死锁。

所以，对事务表（如InnoDB表）使用LOCK TABLES的正确方法是，设置AUTOCOMMIT=0并且不能调用UNLOCK TABLES，直到您明确地提交事务为止。

对于非事务表，则需要使用lock锁定，使用unlock解锁。

### READ锁定

该线程（和所有其它线程）只能从该表中读取。READ LOCAL和READ之间的区别是，READ LOCAL允许在锁定被保持时，执行非冲突性INSERT语句（同时插入）。对于InnoDB表，READ LOCAL与READ相同。

### WRITE锁定

只有保持锁定的线程可以对表进行写入。其它的线程被阻止，直到锁定被释放时为止。

WRITE锁定通常比READ锁定拥有更高的优先权,这意味着，如果一个线程获得了一个READ锁定之后，当另一个线程申请WRITE锁定，则后续的READ锁定申请会等待，直到WRITE线程获得锁定并释放锁定。

如果使用一个读取和一个写入锁定对一个表进行锁定，写入锁定在读取锁定之前执行。

使用LOCK TABLES后，在后面的语句中，将只能使用lock tables语句锁定的表：

	mysql> LOCK TABLE t AS myalias READ;#锁定了表myalias，但是没有锁定表t
	mysql> SELECT * FROM t;#不能使用未锁定的表t
	ERROR 1100: Table 't' was not locked with LOCK TABLES
	不能在一次查询中多次使用一个已锁定的表：
	mysql> LOCK TABLE t WRITE;#表t被锁定
	mysql> INSERT INTO t SELECT * FROM t;#不能在一次查询中多次使用一个已锁定的表
	ERROR 1100: Table 't' was not locked with LOCK TABLES

	#解决办法：在锁定中锁定别名表，在查询中使用别名表
	mysql> LOCK TABLE t WRITE, t AS t1 WRITE;
	mysql> INSERT INTO t SELECT * FROM t AS t1;

	#错误的用法：
	mysql> LOCK TABLE t WRITE;
	mysql> INSERT INTO t SELECT * FROM t AS t1;#别名表t1没有被锁定，所以不能插入

SET TRANSACTION语法
------------

> SET [GLOBAL | SESSION] TRANSACTION ISOLATION LEVEL{ READ UNCOMMITTED | READ COMMITTED | REPEATABLE READ | SERIALIZABLE }

> 本语句用于设置事务隔离等级，用于下一个事务，或者用于当前会话。

在没有使用GLOBAL 或SESSION情况下，SET TRANSACTION会为下一个事务（还未开始）设置隔离等级。

如果您使用GLOBAL关键词，则语句会设置全局性的默认事务等级，用于从该点以后创建的所有新连接。原有的连接不受影响。需要super user权限。使用SESSION关键测可以为当前连接设置默认事务等级，连接关闭后失效。

何时锁定表
=========

通常，您不需要锁定表，因为所有的单个UPDATE语句都是原子性的；没有其它的线程可以干扰任何其它当前正在执行的SQL语句。

但是，在几种情况下，锁定表会有好处：

* 如果您正在对一组MyISAM表运行许多操作，锁定您正在使用的表，可以快很多。锁定MyISAM表可以加快插入、更新或删除的速度。不利方面是，其他线程无法更新一个READ锁定的表（包括保持锁定的表），也不能访问用WRITE锁定的表（除了保持锁定的表以外）。
* 如果您正在使用MySQL中的一个不支持事务的存储引擎，想要执行两条有一定内在联系的select和update语句（即两条语句执行过程中那个，表不能发生影响它们内在联系的数据变动），您必须使用LOCK TABLES，以防止其他进程在select和update执行之间，修改了数据，破坏了select和update间的内在关系，导致select和update执行出错或不符合预期。
* 对于innodb引擎，默认就是行级锁，不需要锁定表。只有在执行非常大和复杂的事务时；或者事务涉及到多个表，事务失败会造成大量回滚，或可能产生死锁，才有必要使用lock tables

死锁
=========

MyISAM只能使用表级别的锁，所以不会产生死锁。

Innodb默认是行级别锁，在普通情况下也不会有死锁，但在事务中，当自动提交是关闭，例如当发生以下情景时，会产生死锁：

进程A的一个事务正在执行语句a1的同一时刻另一个进程B的事务恰好需要访问被进程A独占锁定的行，这时进程B不得不等待进程A释放锁定的行；随后进程A事务开始执行语句a2，而a2如果要访问的恰好是被进程B的事务独占锁定的行，那么进程A不得不等待进程B释放锁。这样就出现了A等B的同时B也在等A释放行，这就是死锁状态。（就像一个独木桥，两个人都要等对方让路，结果是谁都无法通过）

InnoDB 会自动检测一个事务的死锁并回滚一个或多个事务来防止死锁，同时通过设置锁等待超时参数innodb_lock_wait_timeout来解决InnoDB未检测到的死锁问题。但InnoDB不能检测出由 MySQL 的 LOCK TABLES 语句引起的死锁，因为lock tables的锁是由mysql管理而不是由innodb管理。

通常来说，死锁都是应用设计的问题，通过调整业务流程、数据库对象设计、事务大小，以及访问数据库的SQL语句，绝大部分死锁都可以避免：

* 如果你正使用锁定读（SELECT ... FOR UPDATE或 ... LOCK IN SHARE MODE），试着用更低的隔离级别，比如READ COMMITTED。
* 使用更少的锁定。如果你可以接受允许一个SELECT从一个旧的快照返回数据，不要给它添加FOR UPDATE或LOCK IN SHARE MODE子句。
* 添加精心选定的索引到你的表。则你的查询需要扫描更少的索引记录并且因此设置更少的锁定。使用EXPLAIN SELECT来确定对于你的查询,MySQL认为哪个索引是最适当的。（1）在应用中，如果不同的程序会并发存取多个表，应尽量约定以相同的顺序来访问表，这样可以大大降低产生死锁的机会。
* 在程序以批量方式处理同一个表的数据的时候，如果事先处理的数据顺序排序，保证每个线程按固定的顺序来处理记录，也可以大大降低出现死锁的可能。

避免死锁的技巧：http://book.51cto.com/art/200803/68132.htm

尽管通过设计和SQL优化等措施，可以大大减少死锁，但死锁很难完全避免。因此，在程序设计中总是捕获并处理死锁异常是一个很好的编程习惯。

如果出现死锁，可以用 `SHOW ENGINE INNODB STATUS`命令来确定最后一个死锁产生的原因。返回结果中包括死锁相关事务的详细信息，如引发死锁的SQL语句，事务已经获得的锁，正在等待什么锁，以及被回滚的事务等。据此可以分析死锁产生的原因和改进措施。

Innodb的锁定
============

innodb锁定模式
-------

InnoDB实现标准行级锁定，在这里有两种类型的锁：

* 共享的(S)锁允许一个事务去读一行（tuple），同时准许其他事务申请共享锁，但拒绝其他事务申请独占锁。
* 独占的锁(X)允许一个事务更新或删除一行，同时拒绝所有其他事务的任何类型锁申请。

隔离级别
----------

### READ COMMITTED

所有SELECT ... FOR UPDATE和SELECT ... LOCK IN SHARE MOD语句仅锁定索引记录，而不锁定记录间的间隙，因而允许随意紧挨着已锁定的记录插入新记录。UPDATE和DELETE语句使用一个带唯一搜索条件的唯一的索引仅锁定找到的索引记录，而不包括记录间的间隙。在范围类型UPDATE和DELETE语句，InnoDB必须对范围覆盖的间隙设置next-key锁定或间隙锁定以阻止其它用户做插入。

### REPEATABLE READ

这是InnoDB的默认隔离级别。带唯一搜索条件使用唯一索引的SELECT ... FOR UPDATE, SELECT ... LOCK IN SHARE MODE, UPDATE 和DELETE语句只锁定找到的索引记录，而不锁定记录间的间隙。用其它搜索条件，这些操作采用next-key锁定或者间隙锁定锁住搜索的索引范围，并且阻止其它用户的新插入。

### SERIALIZABLE

这个级别类似REPEATABLE READ，但是所有无格式SELECT语句被 隐式转换成SELECT ... LOCK IN SHARE MODE。

InnoDB 中各 SQL 语句的锁定情况
----


`SELECT ... FROM ... ` :  这是一个 consistent read，不以锁定方式读取数据库的快照，除非事务的隔离级被设置为 SERIALIZABLE，在这种情况下将在它所读取的记录索引上设置共享的 next-key locks。

`SELECT ... FROM ... LOCK IN SHARE MODE` : 在所读取的所有记录索引上设置同享的next-key锁定。

`SELECT ... FROM ... FOR UPDATE` : 在所读取的所胡记录索引上设置独占的next-key锁定。

`INSERT  INTO ... VALUES (...)` : 在插入的记录行上设置一个独占地锁定(不是next-key)。如果产生一个重复键值错误， 在重复索引记录上设置一个共享的锁定。如果唯一键没有冲突，REPLACE象一个插入一样被做。另外，对必须更新的行设置一个独占的next-key锁定。

`UPDATE ... WHERE ... ` : 对搜索遇到的每个记录设置一个独占next-key锁定。

`DELETE FROM ... WHERE ...` : 对搜索遇到的每个记录设置一个独占next-key锁定 


KILL [CONNECTION | QUERY] thread_id
---------------------

每个与mysqld的连接都在一个独立的线程里运行，您可以使用 **SHOW PROCESSLIST** 语句查看哪些线程正在运行，并使用KILL thread_id语句终止一个线程。

KILL允许自选的 CONNECTION 或 QUERY修改符： KILL CONNECTION与不含修改符的KILL一样：它会终止与给定的thread_id有关的连接。· KILL QUERY会终止连接当前正在执行的语句，但是会保持连接的原状。 

官方文档
====

[事务和锁定语句](http://dev.mysql.com/doc/refman/5.1/zh/sql-syntax.html#transactional-commands)

[innodb事务模型和锁定](http://dev.mysql.com/doc/refman/5.1/zh/storage-engines.html#innodb-transaction-model)

[innodb性能优化](http://dev.mysql.com/doc/refman/5.1/zh/storage-engines.html#innodb-tuning)
