删除元素
==========

* `$("div").remove()`    从DOM删除元素，返回被删除的元素对象（仅含有对象本身，绑定的事件和附加的数据都被删除了）
* `$("div").detach()`    从DOM中删除元素，但可以把完整的元素对象保存到一个变量中（及其绑定的事件和数据）供继续使用。
* `$("p").remove(".hello")`    删除选择的元素中class为hello的p元素
* `$("p").detach(".hello")`   
* `$('p').empty()`    删除选择的p元素的所有子元素


替换元素
==========

> **replaceWith，replaceAll**方法是直接操作DOM的，所以替换后，被替换掉的元素将不复存在。

```javascript
$("h2").replaceWith("<h1>My Menu</h1>");
$("<b>Paragraph. </b>").replaceAll("p");
//把所有p标记元素换成<b>Paragraph. </b>
```

插入兄弟元素
===========

> 如果被替换的内容还有用，可以插入元素，然后用detach方法移除目标元素，并把detach返回值保存在变量中。

```javascript
$(".meat").before("<li>Tofu1</li>");//在选择的元素之前插入DOM内容（互为兄弟元素）
$(".meat").after("<li>Tofu2</li>");//在选择的元素之前插入DOM内容（互为兄弟元素）
//上面两句等效于
$('<li>Tofu2</li>').insertAfter('.meat');
$('<li>Tofu1</li>').insertBefore('.meat');

```



插入子元素
============


```javascript
$('#errors').append('<p>There are four errors in this form</p>');//添加为最后一个子元素
$('#errors').prepend('<p>There are four errors in this form</p>');//添加为第一个子元素
```


包裹元素
==========


* `wrap(html|elem|fn)`     把匹配的元素的每一个都用参数提供的DOM结构包围
* `unwrap()`    删除每一个匹配元素的父元素
* `wrapInner(html|elem|fn)`     把每一个匹配的元素的内容用参数提供的DOM结构包围
* `wrapAll(html|elem)`     把所有匹配的元素作为整体用参数提供的DOM包围

克隆元素
==============

深度克隆一个jquery对象

clone([even[,deepEven]])

even
: 是否在克隆元素的时候，同时克隆元素上附加的事件,默认为false

deepEven
: 默认取even的值作为默认值，释放克隆事件的时候对子元素的事件也克隆

获取和插入内容
============

```javascript
$('p').html();//获取元素内部的html代码内容（该方法只能用于html,xhtml文档；不能用于xml文档）
$('#errors').html('<p>There are four errors in this form</p>');
$('p').text();//获取p元素下所有文本节点(包括后代元素)的文本内容。
$('#errors h2').text('No errors found');
```