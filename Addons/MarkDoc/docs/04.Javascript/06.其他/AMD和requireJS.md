AMD
=========


> AMD是"Asynchronous Module Definition"的缩写，意思就是"异步模块定义"。
> 它采用异步方式加载模块，模块的加载不影响它后面语句的运行。
> 所有依赖这个模块的语句，都定义在一个回调函数中，等到加载完成之后，这个回调函数才会运行。

> 目前，主要有两个Javascript库实现了AMD规范：require.js和curl.js

> 载入AMD模块: `require([module], callback);`,第一个参数是依赖的模块,不需要写.js后缀,第二个参数是载入完成后执行的回调函数

符合AMD规范的库
---------------

* jQuery 1.7
* Dojo 1.7
* EmbedJS

模块定义规范
------------

> `define([module-name?], [array-of-dependencies?], [module-factory-or-object]);`

> * module-name: 字符串,模块标识，可以省略。
> * array-of-dependencies: 数组,所依赖的模块，可以省略。每一个元素都是一个指定了模块路径的字符串(可以省略.js后缀)
> * module-factory-or-object: 模块的工厂方法实现，或者一个JavaScript对象。

当define函数执行时，它首先会异步的去调用第二个参数中列出的依赖模块，当所有的模块被载入完成之后，
如果第三个参数是一个回调函数则执行，然后告诉系统模块可用，也就通知了依赖于自己的模块自己已经可用。

```
define("alpha", [ "exports", "beta"], function (exports, beta) {
    //预加载所有依赖的模块
　　exports.verb = function() {
    　　return beta.verb();
　　}
});
```

或者在回调函数内部require依赖的模块,在执行到需要依赖的代码时才载入依赖的模块,这样你必须把 **requrie** 定义为依赖的模块

```
define("alpha", ["require", "exports"], function (require, exports) {
　　exports.verb = function() {
    　　return require("beta").verb();//惰性加载依赖的beta模块
　　}
});
```

匿名模块
--------

> define 方法允许你省略第一个参数，这样就定义了一个匿名模块，这时候模块文件的文件名就是模块标识。
> 如果这个模块文件放在a.js中，那么a就是模块名。可以在依赖项中用"a"来依赖于这个匿名模块。
> 这带来一个好处，就是模块是高度可重用的。你拿来一个匿名模块，随便放在一个位置就可以使用它，
> 模块名就是它的文件路径。这也很好的符合了DRY（Don't Repeat Yourself）原则。

下面的代码就定义了一个依赖于alpha模块的匿名模块：

```
define(["alpha"], function (alpha) {
　　return {
    　　verb: function(){
        　　return alpha.verb() + 2;
    　　}
　　}; 
});
```


仅有一个参数的define
-------------------

> define的前两个参数都是可以省略的。第三个参数有两种情况，一种是一个JavaScript对象，另一种是一个函数。

> 如果是一个对象，那么它可能是一个包含方法具有功能的一个对象；也有可能是仅提供数据。后者和JSON-P非常类似，
> 因此AMD也可以认为包含了一个完整的 JSON-P实现。模块演变为一个简单的数据对象，这样的数据对象是高度可用的，
> 而且因为是静态对象，它也是CDN友好的，可以提高JSON-P的性能。考虑一个提供中国省市对应关系的JavaScript对象，
> 如果以传统JSON-P的形式提供给客户端，它必须提供一个callback函数名，根据这个函数名动态生成返回数据，
> 这使得标准JSON-P数据一定不是CDN友好的。

> 但如果用AMD，这个数据文件就是如下的形式：

```
define({
　　provinces: [
    　　{
        　　name: '上海',
        　　areas: ['浦东新区', '徐汇区']},
    　　{
        　　name: '江苏',
        　　cities: ['南京', '南通']} 
        　　//.....
　　]
});
```

假设这个文件名为china.js，那么如果某个模块需要这个数据，只需要：

```
define(['china'], function(china){
　　//在这里使用中国省市数据
});
```

通过这种方式，这个模块是真正高度可复用的，无论是用远程的，还是Copy到本地项目，都节约了开发时间和维护时间。

> 如果参数是一个函数，其用途之一是快速开发实现。适用于较小型的应用，你无需提前关注自己需要什么模块，自己给谁用。

> 在函数中，可以随时require自己需要的模块。例如：

```
define(function(require){
　　var p = require('china');
　　//使用china这个模块
});
```

即你省略了模块名，以及自己需要依赖的模块。这不意味着你无需依赖于其他模块，而是可以让你在需要的时候去require这些模块。
define方法在执行的时候，会调用函数的toString方法，并扫描其中的require调用，提前帮助你载入这些模块，载入完成之后再执行。
这使得快速开发成为可能。需要注意的一点是，Opera不能很好的支持函数的toString方法，因此，在浏览器中它的适用性并不是很强。
但如果你是通过build工具打包所有的 JavaScript文件，这将不是问题，构建工具会帮助你扫描require并强制载入依赖的模块。


requrie.js
==========

> require.js用来加载符合AMD规范的js模块

载入方式
--------

* 同步方式载入require.js: `<script src="js/require.js"></script>`
* 异步方式载入require.js: `<script src="js/require.js" defer async="true" ></script>`

主模块
------

> 主模块是require.js载入完成之后,载入的第一个模块,可以省略后缀名,例如下面的代码表示载入主模块:js/main.js

> `<script src="js/require.js" data-main="js/main"></script>`

> 主模块类似C语言中的main函数,所有的代码都从这里开始执行,最外层只有一个require(),没有define()

例如 main.js 内容如下:

```
require(['jquery', 'underscore', 'backbone'], function ($, _, Backbone){
　　　　// some code here
});
```


模块载入配置
------------

> 默认情况下，require.js假定这依赖的模块与当前js文件在同一个目录，文件名分别为jquery.js，underscore.js和backbone.js，然后自动加载。

> 如果不是这样,则可以使用`require.config()`方法进行配置

> {r:配置代码必须位于顶层html页,或者没有定义模块的顶层脚本文件,在require()函数之前配置}

```
<script src="scripts/require.js"></script>
<script>
  require.config({
    baseUrl: "/another/path",
    paths: {
        "some": "some/v1.0"
    },
    waitSeconds: 15
  });
  require( ["some/module", "my/module", "a.js", "b.js"],
    function(someModule,    myModule) {
        //code
    }
  );
</script>
```

如果要在require.js载入之前进行配置,可以使用一个名为require的全局变量进行配置:

```
<script>
    var require = {
        deps: ["some/module1", "my/module2", "a.js", "b.js"],
        callback: function(module1, module2) {
            //This callback is optional.
        }
    };
</script>
<script src="scripts/require.js"></script>
```

### 配置选项

* baseUrl 

    寻找模块时的基准目录,如果没有指定,则默认值为载入require.js文件的html页面所在的位置;如果使用了data-main属性,则采用这个属性值所指的文件的位置

    它也可以是另一个域名下的url

* paths

    module:path 方式设置模块名与模块对应的js文件的路径映射,{r:路径不需要加后缀};
    
    除非以/开头或者一个完整的url(带协议),否则均是相对于baseUrl的路径;

    在浏览器中,path可以是数组,元素可以是一个cdn路径和一个本地映射,当cdn请求失败时,请求本地的文件
    
        requirejs.config({
            enforceDefine: true,
            paths: {
                jquery: [
                    'http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min',
                    //If the CDN location fails, load from this location
                    'lib/jquery'
                ]
            }
        });
        require(['jquery'], function ($) {
        });

* shim 

    理论上,requirejs只能加载AMD规范的模块,但通过配置shim,也可以加载非AMD标准的模块{r:不要在shim中配置AMD模块}

    shim对象有三个属性,都是可选(由实际情况确定是否必须设置):
        * exports 字符串,配置这个模块在外部调用时的全局变量名
        * deps 数组,配置这个模块的依赖
        * init 模块初始化函数,主要用来解决可能出现的全局变量冲突问题

    ```
    requirejs.config({
        shim: {
            'backbone': {
                deps: ['underscore', 'jquery'],
                exports: 'Backbone'
            },
            'underscore': {
                exports: '_'
            },
            'foo': {
                deps: ['bar'],
                exports: 'Foo',
                init: function (bar) {
                    return this.Foo.noConflict();
                }
            }
        }
    });
    ```

    对应jquery插件backbone插件,他们并没有全局变量,也不存在全局变量冲突,因此无需设置exports和init
    从而简化为只需要使用数组设置依赖的模块即可:

    ```
    requirejs.config({
        shim: {
            'jquery.colorize': ['jquery'],
            'jquery.scroll': ['jquery'],
            'backbone.layoutmanager': ['backbone']
        }
    });
    ```

* packages

    配置CommonJS的package

* config

    为模块传入的配置对象, config对象的每一个属性的属性名对应模块名(如果配置了packages,也可是CommonJS的package的主模块),属性值是这个模块的配置对象

        requirejs.config({
            config: {
                'bar': {
                    size: 'large'
                },
                'baz': {
                    color: 'blue'
                }
            }
        });
        //bar.js, which uses simplified CJS wrapping:
        define(function (require, exports, module) {
            var size = module.config().size;  //Will be the value 'large'
        });
        //baz.js which uses a dependency array,
        define(['module'], function (module) {
            var color = module.config().color;  //Will be the value 'blue'
        });

* waitSeconds
    
    设置超时事件,超过该时间后放弃载入.设为0禁用,默认为7秒

* context  见后文: Multiversion Support

* deps

    数组,元素是依赖的模块,主要用在require.js载入之前进行配置的情况

* callback

    在deps配置的模块载入完成后执行的回调函数

* enforceDefine

     If set to true, an error will be thrown if a script loads that does not call define() or have a shim exports string value that can be checked.

* xhtml

    If set to true, document.createElementNS() will be used to create script elements.

* urlArgs

    当require.js请求文件时,附加的额外url参数,例如:`urlArgs: "bust=" +  (new Date()).getTime()`



!!!! require.js要求，每个模块是一个单独的js文件。这样的话，如果加载多个模块，
就会发出多次HTTP请求，会影响网页的加载速度。因此，require.js提供了一个[优化工具](http://requirejs.org/docs/optimization.html)，
当模块部署完毕以后，可以用这个工具将多个模块合并在一个文件中，减少HTTP请求数。

Multiversion Support
--------------------

> 使用Multiversion Support,必须要在依赖中设置'require'

```
<script src="../require.js"></script>
<script>
/////////////////////////////////////////////////////////
var reqOne = require.config({
  context: "version1",
  baseUrl: "version1"
});

reqOne(["require", "alpha", "beta",],
function(require,   alpha,   beta) {
  log("alpha version is: " + alpha.version); //prints 1
  log("beta version is: " + beta.version); //prints 1

  setTimeout(function() {
    require(["omega"],
      function(omega) {
        log("version1 omega loaded with version: " +
             omega.version); //prints 1
      }
    );
  }, 100);
});
/////////////////////////////////////////////////////////
var reqTwo = require.config({
      context: "version2",
      baseUrl: "version2"
    });

reqTwo(["require", "alpha", "beta"],
function(require,   alpha,   beta) {
  log("alpha version is: " + alpha.version); //prints 2
  log("beta version is: " + beta.version); //prints 2

  setTimeout(function() {
    require(["omega"],
      function(omega) {
        log("version2 omega loaded with version: " +
            omega.version); //prints 2
      }
    );
  }, 100);
});
/////////////////////////////////////////////////////////
</script>
```

错误处理
--------

> To detect errors that are not caught by local errbacks, you can override requirejs.onError():

```
requirejs.onError = function (err) {
    console.log(err.requireType);
    if (err.requireType === 'timeout') {
        console.log('modules: ' + err.requireModules);
    }
    throw err;
};
```

载入插件
=========

> requirejs拥有许多插件,可以用来完成一些特殊工作 : [插件列表](https://github.com/jrburke/requirejs/wiki/Plugins)

> 如果你要编写requirejs插件,可以阅读 : [插件文档](http://requirejs.org/docs/plugins.html)

下面列出几个比较重要的插件

- [domReady](http://requirejs.org/docs/api.html#pageload)
  - Waits for the DOM to be ready for modifications. Includes domReady.withResources(), which is RequireJS-specific. However the basic domReady plugin should be usable in other AMD loaders.
- [i18n](http://requirejs.org/docs/api.html#i18n)
  - Localization plugin. Loads dependencies based on the locale.
- [text](http://requirejs.org/docs/api.html#text)
  - Loads dependencies as plain text files.
- [step](https://github.com/requirejs/step)
  - Loads scripts in steps. Only works with AMD loaders that support [module config](http://requirejs.org/docs/api.html#config-moduleconfig), like RequireJS 2.0. Prefer the RequireJS 2.0 [shim config](http://requirejs.org/docs/api.html#config-shim) over this when possible. 
- [order](http://requirejs.org/docs/api.html#order) 
  - Evaluate scripts in sequence. Useful for non-AMD modules which should be loaded in sequence. Only works with RequireJS, not other AMD loaders. **Only works in RequireJS 1.0, not in RequireJS 2.0**. Use [shim config](http://requirejs.org/docs/api.html#config-shim) or the step plugin instead.

- [image](https://github.com/millermedeiros/requirejs-plugins)
  - Loads image files.
- [json](https://github.com/millermedeiros/requirejs-plugins)
  - Loads JSON files and parses it. Converts it into JS during optimization.
- [mdown](https://github.com/millermedeiros/requirejs-plugins/)
  - Loads Markdown files and compiles into HTML, will also work during optimization.
- [require-css](https://github.com/guybedford/require-css)
  - Fully compatible CSS requiring and build optimization.
- [require-less](https://github.com/guybedford/require-less)
  - Extends require-css, allowing for LESS injection and builds.
- [cs](https://github.com/jrburke/require-cs)
  - Load CoffeeScript files and also compiles modules into JavaScript during optimization.
- [font](https://github.com/millermedeiros/requirejs-plugins)
  - Loads web fonts using the [WebFont Loader API](https://code.google.com/apis/webfonts/docs/webfont_loader.html).
- [use](http://tbranyen.com/post/amdrequirejs-shim-plugin-for-loading-incompatible-javascript) by Tim Branyen
 - Load JS files that aren't wrapped into define calls with the option to set dependencies and the value that should be used by the module.
- [wrap](https://github.com/geddesign/wrapjs) by Dave Geddes
 - Wrap regular scripts as AMD modules.


使用CommonJS模块
================

> 大多是CJS模块都兼容于require,可以直接通过

配置
----

> 配置对象的 package属性的值有以下三个属性

> * name: 包的名字
> * location: 包的位置(相对于baseUrl或绝对路径)
> * main: 包的主模块(相对于包的目录),无需.js后缀; 默认值为 main, 如果包的主模块不是main.js则必须配置该项

```
requirejs.config({
    config: {
        'pixie/index': {
            apiKey: 'XJKDLNS'
        }
    },
    //配置了一个pixie包
    //pixie文件夹下的index.js是包的主模块
    packages: [
        {
            name: 'pixie',
            main: 'index'
        }
    ]
});
```
