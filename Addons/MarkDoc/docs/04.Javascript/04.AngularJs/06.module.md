模块
==========

> 一个模块就是一系列配置和代码块的集合，它们是在启动阶段就附加在到应用上的。一个最简单的模块由两类代码块集合组成的：

* 配置代码块 -  get executed during the provider registrations and configuration phase.
Only providers and constants can be injected into configuration blocks. 

* 运行代码块 -get executed after the injector is created and are used to kickstart the application.
Only instances and constants can be injected into run blocks. 

```
angular.module('myModule', []).
  config(function(injectables) { // provider-injector
    // This is an example of config block.
    // You can have as many of these as you want.
    // You can only inject Providers (not instances)
    // into the config blocks.
  }).
  run(function(injectables) { // instance-injector
    // This is an example of a run block.
    // You can have as many of these as you want.
    // You can only inject instances (not Providers)
    // into the run blocks
  });
```

推荐实践
---------

> 推荐将你的应用拆分成以下几个模块：

* 一个服务模块，用来做服务的声明。
* 一个指令模块，用来做指令的声明。
* 一个过滤器模块，用来做过滤器声明。
* 一个依赖以上模块的应用级模块，它包含应用初始化代码。

Configuration Blocks
------------------

> 有一些便捷方法用于配置模块，例如：

```
angular.module('myModule', []).
  value('a', 123).
  factory('a', function() { return 123; }).
  directive('directiveName', ...).
  filter('filterName', ...);
 
// 等效于
 
angular.module('myModule', []).
  config(function($provide, $compileProvider, $filterProvider) {
    $provide.value('a', 123);
    $provide.factory('a', function() { return 123; });
    $compileProvider.directive('directiveName', ...);
    $filterProvider.register('filterName', ...);
  });
```

运行块
--------

> 运行块是AngularJS中最像主方法的东西。一个运行块就是一段用来启动应用的代码。它在所有服务都被配置和所有的注入器都被创建后执行。
运行块通常包含了一些难以测试的代码，所以它们应该写在单独的模块里，这样在单元测试时就可以忽略它们了。


依赖
-------

> 模块可以把其他模块列为它的依赖。“依赖某个模块”意味着需要把这个被依赖的模块需要在本块模块之前被加载。
换句话说被依赖模块的配置块会在本模块配置块前被执行。运行块也是一样。任何一个模块都只能被加载一次，即使它被多个模块依赖。


异步加载
--------

> 模块是一种用来管理$injector配置的方法，和脚本的加载没有关系。现在网上已有很多控制模块加载的项目，它们可以和AngularJS配合使用。
因为在加载期间模块不做任何事情，所以它们可以以任意顺序或者并行方式加载。